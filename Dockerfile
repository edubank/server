FROM openjdk:14-jdk-alpine
VOLUME /tmp
ARG JAR_FILE
COPY /target/*.jar app.jar
ENTRYPOINT ["java", "-server", "-XX:+UseContainerSupport", "-jar", "app.jar"]