package edu.kit.informatik.pse.edubank.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import edu.kit.informatik.pse.edubank.entity.Subject;
import lombok.Data;

/**
 * Eine SubjectClassResponseView bündelt Informationen für Antworten bei Fach-Anfragen, wenn die zugehörige Klasse nicht
 * klar ist. Dementsprechend werden neben den gleichen Informationen wie bei einer SubjectResponseView auch noch die
 * Klasse gespeichert, zu der das Fach gehört (schoolClass).
 *
 * @author EduBank Team
 * @version 1.0
 * @see SubjectResponseView
 */
@Data
public class SubjectClassResponseView {
    private Integer id;
    private String name;
    @JsonProperty("class")
    private SchoolClassResponseView schoolClass;
    private UserResponseDTO teacher;

    /**
     * Erzeugt eine neue SubjectClassResponseView.
     *
     * @param subject Das Fach, dessen Informationen gebündelt werden.
     * @since 1.0
     */
    public SubjectClassResponseView(Subject subject) {
        id = subject.getId();
        name = subject.getName();
        schoolClass = subject.getSchoolClass() == null ? null : new SchoolClassResponseView(subject.getSchoolClass());
        teacher = subject.getTeacher() == null ? null : new UserResponseDTO(subject.getTeacher());
    }
}
