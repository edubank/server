package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.request.EventRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.TransactionResponseDTO;
import edu.kit.informatik.pse.edubank.entity.Transaction;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.service.TransactionService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Ein EventController verwaltet alle Anfragen und Zugriffe auf Events. Alle Rückgaben der Methoden werden in einem
 * ResponseEntity verpackt.
 *
 * @author EduBank Team
 * @version 1.0
 */
@RestController
public class EventController {

    private final TransactionService transactionService;

    /**
     * Erstellt einen neuen EventController.
     *
     * @param transactionService Der TransactionsService, der die benötigten Funktionen bereitstellt.
     */
    public EventController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    /**
     * Erstellt ein neues Event.
     *
     * @param eventRequest Das Event, das erstellt werden soll.
     * @param authUser     Der Benutzer, der die Anfrage stellt.
     * @return Eine Liste mit allen Transaktionen, die als Folge ausgeführt wurden
     */
    @PostMapping("/initiate-event")
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<List<TransactionResponseDTO>> initiateEvent(@Valid @RequestBody EventRequestDTO eventRequest,
                                                                      @AuthenticationPrincipal AuthenticatedUser authUser) {
        List<Transaction> transactions = transactionService.initiateEvent(eventRequest.getTitle(), eventRequest.getAccountIds(), eventRequest.getAmount(), authUser.getUser());
        return ResponseEntity.ok(transactions.stream().map(TransactionResponseDTO::new).collect(Collectors.toList()));
    }
}
