package edu.kit.informatik.pse.edubank.service;

import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.User;

import java.util.List;

/**
 * Ein UserService stellt Funktionen für das Arbeiten mit Benutzern zur Verfügung.
 *
 * @author EduBank Team
 * @version 1.0
 */
public interface UserService {

    /**
     * Durchsucht das System nach allen Benutzern.
     *
     * @return Eine Liste mit allen Benutzern des Systems.
     * @since 1.0
     */
    List<User> findAll();

    /**
     * Durchsucht das System nach einem speziellen Benutzer.
     *
     * @param id Die ID des Benutzers.
     * @return Den gesuchten Benutzer, falls dieser im System existiert.
     * @since 1.0
     */
    User findById(Integer id);

    /**
     * Durchsucht das System nach allen Benutzern, die zu einem Fach gehören.
     *
     * @param subjectId Die ID des Fachs.
     * @return Eine Liste mit allen Benutzern des Fachs.
     * @since 1.0
     */
    List<User> findBySubjectId(Integer subjectId);

    /**
     * Durchsucht das System nach allen Benutzern mit einer speziellen Rolle.
     *
     * @param role Die Rolle, nach der gefiltert werden soll.
     * @return Eine Liste mit allen Benutzern, die die gegebene Rolle haben.
     * @since 1.0
     */
    List<User> findByRole(User.UserRole role);

    /**
     * Legt einen neuen Benutzer im System an.
     *
     * @param user Der anzulegende Benutzer.
     * @return Der neu angelegte Benutzer.
     * @since 1.0
     */
    User createUser(User user);

    /**
     * Nimmt Änderungen an einem existierenden Benutzer vor.
     *
     * @param id   Die ID des Benutzers, der geändert werden soll.
     * @param user Der geänderte Benutzer.
     * @return Der im System geänderte Benutzer.
     * @since 1.0
     */
    User updateUser(Integer id, User user);

    /**
     * Entfernt einen Benutzer aus dem System.
     *
     * @param id Die ID des Benutzers, der entfernt werden soll.
     * @return 'true' falls der Benutzer erfolgreich entfernt wurde, sonst 'false'.
     * @since 1.0
     */
    boolean deleteUserById(Integer id);

    /**
     * Setzt das Passwort eines Benutzers zurück und generiert ein neues.
     *
     * @param email Die E-Mail vom Benutzer, dessen Passwort zurückgesetzt werden soll.
     * @since 1.0
     */
    void resetPassword(String email);
}
