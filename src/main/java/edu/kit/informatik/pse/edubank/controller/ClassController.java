package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.request.SchoolClassRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.SchoolClassResponseDTO;
import edu.kit.informatik.pse.edubank.entity.SchoolClass;
import edu.kit.informatik.pse.edubank.service.ClassService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Ein ClassController verwaltet alle Anfragen und Zugriffe auf Klassen. Alle Rückgaben der Methoden werden in einem
 * ResponseEntity verpackt.
 *
 * @author EduBank Team
 * @version 1.0
 */
@RestController
@RequestMapping("/classes")
public class ClassController {

    private final ClassService classService;

    /**
     * Erstellt einen neuen ClassController.
     *
     * @param classService Der ClassService, der die benötigten Funktionen bereitstellt.
     * @since 1.0
     */
    public ClassController(ClassService classService) {
        this.classService = classService;
    }

    /**
     * Durchsucht das System nach allen Klassen.
     *
     * @return Eine Liste mit allen Klassen, die im System existieren.
     * @since 1.0
     */
    @GetMapping
    @Secured("ROLE_ADMIN")
    public ResponseEntity<List<SchoolClassResponseDTO>> findAll() {
        List<SchoolClass> results = classService.findAll();
        return ResponseEntity.ok(results.stream().map(SchoolClassResponseDTO::new).collect(Collectors.toList()));
    }

    /**
     * Durchsucht das System nach einer Klasse mit einer gegeben ID.
     *
     * @param id Die ID, nach der gesucht werden soll.
     * @return Die gesuchte Klasse, falls sie existiert.
     * @since 1.0
     */
    @GetMapping("/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<SchoolClassResponseDTO> findById(@PathVariable Integer id) {
        SchoolClass schoolClass = classService.findById(id);
        if (schoolClass != null) {
            return ResponseEntity.ok(new SchoolClassResponseDTO(schoolClass));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    /**
     * Legt eine neue Klasse im System an.
     *
     * @param schoolClass Die Klasse, die angelegt werden soll.
     * @return Die neu erstellte Klasse.
     * @since 1.0
     */
    @PostMapping
    @Secured("ROLE_ADMIN")
    public ResponseEntity<SchoolClassResponseDTO> createClass(@Valid @RequestBody SchoolClassRequestDTO schoolClass) {
        SchoolClass result = classService.createClass(schoolClass.toEntity());
        return ResponseEntity.ok(new SchoolClassResponseDTO(result));
    }

    /**
     * Nimmt Änderungen an einer bereits existierenden Klasse vor.
     *
     * @param id          Die ID der Klasse, die geändert werden soll.
     * @param schoolClass Die geänderte Klasse.
     * @return Die neue, geänderte Klasse.
     * @since 1.0
     */
    @PutMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<SchoolClassResponseDTO> updateClass(@PathVariable Integer id,
                                                              @Valid @RequestBody SchoolClassRequestDTO schoolClass) {
        SchoolClass result = classService.updateClass(id, schoolClass.toEntity());
        return ResponseEntity.ok(new SchoolClassResponseDTO(result));
    }

    /**
     * Löscht eine existierende Klasse aus dem System.
     *
     * @param id Die ID der Klasse, die gelöscht werden soll.
     * @return 'true', wenn die Klasse gelöscht wurde. Sonst 'false'.
     * @since 1.0
     */
    @DeleteMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Boolean> deleteClass(@PathVariable Integer id) {
        if (classService.deleteClassById(id)) {
            return ResponseEntity.ok(true);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(false);
        }
    }

}
