package edu.kit.informatik.pse.edubank.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Ein EventRequestDTO bündelt alle für eine Event-Anfrage relevante Daten. Das beinhaltet den Titel, also den Namen,
 * des Events (title). Die betroffenen Konten, in Form der jeweiligen IDs (accountsIds) und den Geldbetrag (amount).
 * Jeder dieser Werte muss gesetzt sein, bzw. im Falle der Konten muss mindestens ein Konto angegeben sein.
 *
 * @author EduBank Team
 * @version 1.0
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EventRequestDTO {

    @NotBlank(message = "Der Überschrift darf nicht leer sein.")
    private String title;
    @NotNull(message = "Die Liste der Konten darf nicht leer sein.")
    private List<Integer> accountIds;
    @NotNull(message = "Der Betrag des Events darf nicht leer sein.")
    private Double amount;
}
