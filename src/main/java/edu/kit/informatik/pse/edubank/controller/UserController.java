package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.request.UserRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.UserResponseDTO;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.service.SubjectService;
import edu.kit.informatik.pse.edubank.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Ein UserController verwaltet alle Anfragen und Zugriffe auf Benutzer. Alle Rückgaben der Methoden werden in einem
 * ResponseEntity verpackt.
 *
 * @author EduBank Team
 * @version 1.0
 */
@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final SubjectService subjectService;

    /**
     * Erstellt einen neuen UserController.
     *
     * @param userService    Der Service für Benutzer.
     * @param subjectService Der Service für Fächer.
     * @since 1.0
     */
    public UserController(UserService userService, SubjectService subjectService) {
        this.userService = userService;
        this.subjectService = subjectService;
    }

    /**
     * Durchsucht das System nach allen Benutzern.
     *
     * @return Eine Liste aller existierender Benutzer.
     * @since 1.0
     */
    @GetMapping
    @Secured("ROLE_ADMIN")
    public ResponseEntity<List<UserResponseDTO>> findAll() {
        List<User> results = userService.findAll();
        return ResponseEntity.ok(results.stream().map(UserResponseDTO::new).collect(Collectors.toList()));
    }

    /**
     * Durchsucht das System nach allen Benutzern die zu einem gegeben Fach gehören.
     *
     * @param subjectId Die ID des Fachs.
     * @param authUser Der angemeldete Benutzer.
     * @return Eine Liste mit allen Benutzern des Fachs.
     * @since 1.0
     */
    @GetMapping(params = "subject_id")
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<List<UserResponseDTO>> findBySubjectId(@RequestParam("subject_id") Integer subjectId,
                                                                 @AuthenticationPrincipal AuthenticatedUser authUser) {

        Subject subject = subjectService.findById(subjectId);

        if (subject == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        if (!subjectService.hasPermission(authUser.getUser(), subject)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        List<User> results = userService.findBySubjectId(subjectId);
        return ResponseEntity.ok(results.stream().map(UserResponseDTO::new).collect(Collectors.toList()));
    }

    /**
     * Durchsucht das System nach allen Benutzern mit einer gegebenen Rolle.
     *
     * @param role Die Rolle, nach der gefiltert werden soll.
     * @return Eine Liste aller Benutzer mit dieser Rolle.
     * @since 1.0
     */
    @GetMapping(params = "role")
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<List<UserResponseDTO>> findByRole(@RequestParam User.UserRole role) {
        List<User> results = userService.findByRole(role);
        return ResponseEntity.ok(results.stream().map(UserResponseDTO::new).collect(Collectors.toList()));
    }

    /**
     * Durchsucht das System nach einem speziellen Benutzer.
     *
     * @param id Die ID des gesuchten Benutzers.
     * @return Den gesuchten Benutzer, falls er existiert.
     */
    @GetMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<UserResponseDTO> findById(@PathVariable Integer id) {
        User user = userService.findById(id);
        if (user != null) {
            return ResponseEntity.ok(new UserResponseDTO(user));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    /**
     * Legt einen neuen Benutzer im System an.
     *
     * @param user Der Benutzer, der angelegt werden soll.
     * @return Eine Bestätigung, mit den Daten des neuen Benutzers.
     * @since 1.0
     */
    @PostMapping
    @Secured("ROLE_ADMIN")
    public ResponseEntity<UserResponseDTO> createUser(@Valid @RequestBody UserRequestDTO user) {
        User result = userService.createUser(user.toEntity());
        return ResponseEntity.ok(new UserResponseDTO(result));
    }

    /**
     * Nimmt Änderungen an einem existierenden Benutzer vor.
     *
     * @param id   Die ID des Benutzers, der geändert werden soll.
     * @param user Der Benutzer mit den geänderte Daten.
     * @return Eine Bestätigung, mit den neuen Benutzerdaten.
     */
    @PutMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<UserResponseDTO> updateUser(@PathVariable Integer id,
                                                      @Valid @RequestBody UserRequestDTO user) {
        User result = userService.updateUser(id, user.toEntity());
        return ResponseEntity.ok(new UserResponseDTO(result));
    }

    /**
     * Löscht einen Benutzer aus dem System.
     *
     * @param id Die ID des zu löschenden Benutzers.
     * @return 'true', wenn der Vorgang erfolgreich war, sonst 'false'.
     */
    @DeleteMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Boolean> deleteUser(@PathVariable Integer id) {
        if (userService.deleteUserById(id)) {
            return ResponseEntity.ok(true);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(false);
        }
    }

}
