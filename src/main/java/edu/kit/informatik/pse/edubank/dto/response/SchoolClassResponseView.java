package edu.kit.informatik.pse.edubank.dto.response;

import edu.kit.informatik.pse.edubank.entity.SchoolClass;
import lombok.Data;

/**
 * Eine SchoolClassResponseView bündelt Informationen für die Antwort bei einer Klassen-Anfrage. Hierzu zählen die ID
 * der Klasse (id) und der Name der Klasse (name).
 *
 * @author EduBank Team
 * @version 1.0
 * @see SchoolClassResponseDTO
 */
@Data
public class SchoolClassResponseView {
    private Integer id;
    private String name;

    /**
     * Erzeugt eine neue SchoolClassResponseView.
     *
     * @param schoolClass Die Klasse, deren Informationen gebündelt werden.
     * @since 1.0
     */
    public SchoolClassResponseView(SchoolClass schoolClass) {
        id = schoolClass.getId();
        name = schoolClass.getName();
    }
}