package edu.kit.informatik.pse.edubank.dto.response;

import edu.kit.informatik.pse.edubank.entity.User;
import lombok.Data;

/**
 * Eine UserResponseDTO bündelt Informationen für Antworten bei Benutzer-Anfragen. Hierzu zählen die ID des Benutzers
 * (id), der Benutzername (username), das generierte Passwort (passwordGen), die Rolle (role), der Vorname (firstName),
 * der Nachname (lastName) und die E-Mail-Adresse (email).
 *
 * @author EduBank Team
 * @version 1.0
 */
@Data
public class UserResponseDTO {
    private Integer id;
    private String username;
    private String passwordGen;
    private User.UserRole role;
    private String firstName;
    private String lastName;
    private String email;

    /**
     * Erzeugt ein neues UserResponseDTO.
     *
     * @param user Der Benutzer, dessen Informationen gebündelt werden.
     * @since 1.0
     */
    public UserResponseDTO(User user) {
        id = user.getId();
        username = user.getUsername();
        passwordGen = user.getPasswordGen();
        role = user.getRole();
        firstName = user.getFirstName();
        lastName = user.getLastName();
        email = user.getEmail();
    }
}
