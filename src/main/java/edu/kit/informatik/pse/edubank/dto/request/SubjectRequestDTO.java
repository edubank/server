package edu.kit.informatik.pse.edubank.dto.request;

import edu.kit.informatik.pse.edubank.entity.SchoolClass;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Ein SubjectRequestDTO bündelt die Informationen für eine Anfrage, die ein Fach betrifft. Das beinhaltet den Namen
 * (name) des
 * Fachs, die ID der Klasse - zu dem das Fach gehört - (classId) und die ID des Lehrers, welcher die Klasse verwaltet
 * (teacherId).
 *
 * @version 1.0
 * @author EduBank Team
 */
@Data
@Builder
@AllArgsConstructor
public class SubjectRequestDTO {

    @NotBlank(message = "Ein gültiger Name ist erforderlich.")
    private String name;
    private Integer classId;
    @NotNull(message = "Ein gültiger Lehrer ist erforderlich.")
    private Integer teacherId;

    /**
     * Diese Methode erzeugt aus den im Objekt gespeicherten Informationen über das Fach genau dieses beschriebene Fach.
     *
     * @return Ein {@link Subject} das genau die Werte enthält, die im DTO gespeichert sind.
     * @since 1.0
     */
    public Subject toEntity() {
        return Subject.builder()
                .name(name)
                .schoolClass(classId == null ? null : SchoolClass.builder().id(classId).build())
                .teacher(User.builder().id(teacherId).build())
                .build();
    }
}
