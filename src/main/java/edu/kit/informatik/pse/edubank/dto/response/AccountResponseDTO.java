package edu.kit.informatik.pse.edubank.dto.response;

import edu.kit.informatik.pse.edubank.entity.Account;
import lombok.Data;

/**
 * Ein AccountResponseDTO bündelt die gleichen Informationen wie eine AccountResponseView und zusätzlich noch das Fach
 * (subject), zu dem das Konto gehört.
 *
 * @author EduBank Team
 * @version 1.0
 * @see AccountResponseView
 */
@Data
public class AccountResponseDTO {
    private Integer id;
    private SubjectClassResponseView subject;
    private UserResponseDTO student;
    private Double balance;

    /**
     * Erzeugt ein neues AccountResponseDTO.
     *
     * @param account Das Konto, dessen Informationen im DTO verwendet werden.
     * @since 1.0
     */
    public AccountResponseDTO(Account account) {
        id = account.getId();
        subject = new SubjectClassResponseView(account.getSubject());
        student = new UserResponseDTO(account.getStudent());
        balance = account.getBalance();
    }
}
