package edu.kit.informatik.pse.edubank.repository;

import edu.kit.informatik.pse.edubank.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Ein UserRepository bündelt alle Benutzer des Systems und bietet Funktionen, nach Gruppen von Benutzern zu suchen.
 *
 * @version 1.0
 * @author EduBank Team
 */
public interface UserRepository extends JpaRepository<User, Integer> {

    /**
     * Diese Funktion gibt die Benutzer zurück, denen eine der eingegebenen IDs zugeordnet ist.
     *
     * @param ids Die IDs, nach denen gesucht werden soll.
     * @return Die Benutzer mit den eingegebenen IDs.
     * @since 1.0
     */
    List<User> findByIdIn(List<Integer> ids);

    /**
     * Diese Funktion gibt den Benutzer zurück, dem der gegebene Benutzername zugeordnet ist, falls dieser existiert.
     *
     * @param username Der Benutzername, nach dem gesucht werden soll.
     * @return Den Benutzer, falls dieser im System existiert.
     * @since 1.0
     */
    Optional<User> findByUsername(String username);

    /**
     * Diese Funktion gibt den Benutzer zurück, dem die gegebene E-Mail zugeordnet ist, falls dieser existiert.
     *
     * @param email Die E-Mail, nach der gesucht werden soll.
     * @return Den Benutzer, falls dieser im System existiert.
     * @since 1.0
     */
    Optional<User> findByEmail(String email);

    /**
     * Diese Methode gibt alle Benutzer zurück, die Teil des gegebenen Fachs sind.
     *
     * @param subjectId Die ID des Fachs, nach dessen Benutzern gesucht werden soll.
     * @return Eine Liste aller Benutzer, die zum gegebenen Fach gehören.
     * @since 1.0
     */
    List<User> findBySubjects_Id(Integer subjectId);

    /**
     * Gibt eine Liste aller Benutzer, die eine bestimme Rolle haben.
     *
     * @param role Die Rolle, nach der gefiltert werden soll.
     * @return Eine Liste aller Benutzer mit dieser Rolle.
     * @since 1.0
     */
    List<User> findByRole(User.UserRole role);
}
