package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.request.ForgotPasswordRequestDTO;
import edu.kit.informatik.pse.edubank.dto.request.LoginRequestDTO;
import edu.kit.informatik.pse.edubank.dto.request.ProfileDataRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.UserResponseDTO;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.security.SecurityConfiguration;
import edu.kit.informatik.pse.edubank.service.UserService;
import edu.kit.informatik.pse.edubank.service.impl.ValidationException;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

/**
 * Ein LoginController verwaltet alle Anfragen in Bezug auf Anmeldungen. Alle Rückgaben der Methoden werden in einem
 * ResponseEntity verpackt.
 *
 * @author EduBank Team
 * @version 1.0
 */
@Import(SecurityConfiguration.class)
@RestController
public class LoginController {

    private final AuthenticationManager authenticationManager;
    private final UserService userService;

    /**
     * Erstellt einen neuen LoginController.
     *
     * @param authenticationManager Der AuthenticationManager, der Anfragen zur Authenfizierung bearbeitet.
     * @param userService Der Benutzer-Service.
     * @since 1.0
     */
    public LoginController(AuthenticationManager authenticationManager, UserService userService) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
    }

    /**
     * Stellt eine Login-Anfrage an das System.
     *
     * @param login   Die Login-Daten.
     * @param session Die Anmelde-Session.
     * @return Eine Bestätigung, falls die Anmeldung erfolgreich war.
     * @since 1.0
     */
    @PostMapping("/login")
    @PermitAll
    public ResponseEntity<String> login(@Valid @RequestBody LoginRequestDTO login, HttpSession session) {
        UsernamePasswordAuthenticationToken authToken =
                new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword());

        Authentication authentication = authenticationManager.authenticate(authToken);
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authentication);
        session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, securityContext);
        return ResponseEntity.ok(null);
    }

    @PostMapping("/logout")
    @PermitAll
    public ResponseEntity<String> logout() {
        return ResponseEntity.ok("Logged out.");
    }

    /**
     * Setzt die Daten, mit denen ein neuer Benutzer initialisiert wird.
     *
     * @param profileData Die Benutzerdaten.
     * @param authUser    Der Benutzer, der die Anfrage stellt.
     * @return Eine Bestätigung, falls der Vorgang erfolgreich war.
     * @since 1.0
     */
    @PostMapping("/initial-data")
    @PermitAll
    public ResponseEntity<UserResponseDTO> setInitialData(@Valid @RequestBody ProfileDataRequestDTO profileData,
                                                          @AuthenticationPrincipal AuthenticatedUser authUser) {
        authUser.getUser().setPassword("");

        if (profileData.getPassword().isEmpty()) {
            throw new ValidationException("password", "Ein neues Passwort ist erforderlich.");
        }

        if (profileData.getEmail().isEmpty()) {
            throw new ValidationException("email", "Eine E-Mail ist erforderlich.");
        }

        authUser.getUser().setPassword(profileData.getPassword());
        authUser.getUser().setEmail(profileData.getEmail());

        User result = userService.updateUser(authUser.getUser().getId(), authUser.getUser());
        return ResponseEntity.ok(new UserResponseDTO(result));
    }

    /**
     * Stellt eine Anfrage, dass das Passwort vergessen wurde. Dadurch wird das aktuelle Passwort mit einem neuen,
     * generierten Passwort überschrieben und an den Benutzer per Mail geschickt.
     *
     * @param forgotPasswordRequest Die Anfrage.
     * @return Eine Bestätigung, dass das Passwort zurückgesetzt wurde.
     * @since 1.0
     */
    @PostMapping("/forgot-password")
    @PermitAll
    public ResponseEntity<String> forgotPassword(@Valid @RequestBody ForgotPasswordRequestDTO forgotPasswordRequest) {
        userService.resetPassword(forgotPasswordRequest.getEmail());
        return ResponseEntity.ok("Passwort wurde zurückgesetzt.");
    }

}
