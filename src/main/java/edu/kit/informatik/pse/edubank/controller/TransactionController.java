package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.response.TransactionResponseDTO;
import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.Transaction;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.service.AccountService;
import edu.kit.informatik.pse.edubank.service.TransactionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Ein TransactionController verwaltet alle Anfragen und Zugriffe auf Transaktionen. Alle Rückgaben der Methoden werden
 * in einem ResponseEntity verpackt.
 *
 * @author EduBank Team
 * @version 1.0
 */
@RestController
@RequestMapping("/transactions")
public class TransactionController {

    private final TransactionService transactionService;
    private final AccountService accountService;

    /**
     * Erstellt einen neuen TransactionController.
     *
     * @param transactionService Der Service für Transaktionen.
     * @param accountService     Der Service für Konten.
     * @since 1.0
     */
    public TransactionController(TransactionService transactionService, AccountService accountService) {
        this.transactionService = transactionService;
        this.accountService = accountService;
    }

    /**
     * Durchsucht das System nach einer Transaktion.
     *
     * @param id       Die ID der Transaktion.
     * @param authUser Der Benutzer, der die Anfrage stellt.
     * @return Die gesuchte Transaktion, falls diese existiert.
     * @since 1.0
     */
    @GetMapping("/{id}")
    @PermitAll
    public ResponseEntity<TransactionResponseDTO> findById(@PathVariable Integer id,
                                                           @AuthenticationPrincipal AuthenticatedUser authUser) {
        Transaction transaction = transactionService.findById(id);

        if (transaction == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        if (!accountService.hasPermission(authUser.getUser(), transaction.getAccount())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        return ResponseEntity.ok(new TransactionResponseDTO(transaction));
    }

    /**
     * Durchsucht das System nach allen Transaktionen, die zu einem Konto gehören.
     *
     * @param accountId Die ID des Kontos.
     * @param authUser  Der Benutzer, der die Anfrage stellt.
     * @return Eine Liste mit allen Transaktionen, die auf dem Konto durchgeführt wurden.
     */
    @GetMapping(params = "account_id")
    @PermitAll
    public ResponseEntity<List<TransactionResponseDTO>> findByAccountId(@RequestParam("account_id") Integer accountId,
                                                                        @AuthenticationPrincipal AuthenticatedUser authUser) {
        Account account = accountService.findById(accountId);

        if (account == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        if (!accountService.hasPermission(authUser.getUser(), account)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        List<Transaction> results = transactionService.findByAccountId(accountId);
        return ResponseEntity.ok(results.stream().map(TransactionResponseDTO::new).collect(Collectors.toList()));
    }

}
