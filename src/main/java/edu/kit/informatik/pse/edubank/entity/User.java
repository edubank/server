package edu.kit.informatik.pse.edubank.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Ein Benutzer (User) repräsentiert eine echte Person innerhalb des Systems. Jeder Benutzer hat neben seinem Namen
 * (firstName und lastName) auch eine eindeutige ID (id) und einen Benutzernamen (username), die ihn im System eindeutig
 * identifizieren. Zusätzlich hat jeder Benutzer auch eine Rolle (role), die seine Berechtigungen angibt und eine
 * E-Mail-Adresse (email), sowie eine Liste aller seiner Konten (subjects), die ja zu einem Fach (Subject) gehören.
 *
 * @version 1.0
 * @author EduBank Team
 */
@Data
@Builder
@Entity
@Table(name = "users", indexes = @Index(columnList = "username"))
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    /**
     * Eine Benutzerrolle gibt die Freigabe eines Benutzers an und je nach Freigabe stehen dem Benutzer unterschiedliche
     * Funktionen zur Verfügung. Sollte ein Lehrer gleichzeitig Administrator des Systems sein, wird in diesem Fall
     * die höhere Berechtigung, also Administrator, vergeben.
     *
     * @version 1.0
     * @author EduBank Team
     */
    public enum UserRole {
        /**
         * Diese Rolle bekommen Schüler.
         *
         * @since 1.0
         */
        ROLE_STUDENT,
        /**
         * Diese Rolle bekommen Lehrer.
         *
         * @since 1.0
         */
        ROLE_TEACHER,
        /**
         * Diese Rolle bekommen Administratoren.
         *
         * @since 1.0
         */
        ROLE_ADMIN
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true)
    private String username;
    private String passwordGen;
    private String password;
    private UserRole role;
    private String firstName;
    private String lastName;
    @Column(unique = true)
    private String email;

    @OneToMany(mappedBy = "student")
    private List<Account> accounts;

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(
            name = "accounts",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id"))
    private List<Subject> subjects;

}
