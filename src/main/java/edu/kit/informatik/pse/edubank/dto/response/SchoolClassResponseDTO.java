package edu.kit.informatik.pse.edubank.dto.response;

import edu.kit.informatik.pse.edubank.entity.SchoolClass;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Ein SchoolClassResponseDTO bündelt die gleichen Informationen wie eine SchoolClassResponseView und zusätzlich noch
 * eine Liste mit allen Fächern dieser Klasse (subjects).
 *
 * @see SchoolClassResponseView
 * @author EduBank Team
 * @version 1.0
 */
@Data
public class SchoolClassResponseDTO {
    private Integer id;
    private String name;
    private List<SubjectResponseView> subjects;

    /**
     * Erzeugt ein neues SchoolClassResponseDTO.
     *
     * @param schoolClass Die Klasse, deren Informationen gebündelt werden.
     * @since 1.0
     */
    public SchoolClassResponseDTO(SchoolClass schoolClass) {
        id = schoolClass.getId();
        name = schoolClass.getName();
        subjects = schoolClass.getSubjects().stream().map(SubjectResponseView::new).collect(Collectors.toList());
    }
}
