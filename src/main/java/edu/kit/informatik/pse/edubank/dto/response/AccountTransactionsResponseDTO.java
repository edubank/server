package edu.kit.informatik.pse.edubank.dto.response;

import edu.kit.informatik.pse.edubank.entity.Account;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Ein AccountTransactionResponseDTO bündelt Informationen für Antworten bei Transaktions-Anfragen zu Konten. Hierzu
 * zählen die ID des Kontos (id), das Fach der Kontos (subject), der Schüler (student), der aktuelle Kontostand
 * (balance) und eine Liste aller Transaktionen auf dem Konto (transactions).
 *
 * @author EduBank Team
 * @version 1.0
 * @since 1.0
 */
@Data
public class AccountTransactionsResponseDTO {
    private Integer id;
    private SubjectClassResponseView subject;
    private UserResponseDTO student;
    private Double balance;
    private List<TransactionResponseView> transactions;

    /**
     * Erzeugt ein neues AccountTransactionsResponseDTO.
     *
     * @param account Das Konto, dessen Informationen gebündelt werden.
     * @since 1.0
     */
    public AccountTransactionsResponseDTO(Account account) {
        id = account.getId();
        subject = new SubjectClassResponseView(account.getSubject());
        student = new UserResponseDTO(account.getStudent());
        balance = account.getBalance();
        transactions = account.getTransactions().stream().map(TransactionResponseView::new).collect(Collectors.toList());
    }
}
