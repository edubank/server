package edu.kit.informatik.pse.edubank.dto.response;

import edu.kit.informatik.pse.edubank.entity.Account;
import lombok.Data;

/**
 * Eine AccountResponseView bündelt Informationen für Antworten bei Konto-Anfragen. Hierzu zählen die ID des Kontos
 * (id), der Schüler, dem das Konto gehört (student) und der aktuelle Kontostand (balance).
 *
 * @author EduBank Team
 * @version 1.0
 * @see AccountResponseDTO
 */
@Data
public class AccountResponseView {
    private Integer id;
    private UserResponseDTO student;
    private Double balance;

    /**
     * Erzeugt eine neue AccountResponseView.
     *
     * @param account Das Konto, dessen Informationen im DTO verwendet werden.
     * @since 1.0
     */
    public AccountResponseView(Account account) {
        id = account.getId();
        student = new UserResponseDTO(account.getStudent());
        balance = account.getBalance();
    }
}
