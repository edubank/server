package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.request.TransactionRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.AccountResponseDTO;
import edu.kit.informatik.pse.edubank.dto.response.AccountTransactionsResponseDTO;
import edu.kit.informatik.pse.edubank.dto.response.TransactionResponseDTO;
import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.Transaction;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.service.AccountService;
import edu.kit.informatik.pse.edubank.service.SubjectService;
import edu.kit.informatik.pse.edubank.service.TransactionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Ein AccountController verwaltet alle Anfragen und Zugriffe auf Konten. Alle Rückgaben der Methoden werden in einem
 * ResponseEntity verpackt.
 *
 * @author EduBank Team
 * @version 1.0
 */
@RestController
@RequestMapping("/accounts")
public class AccountController {

    private final AccountService accountService;
    private final SubjectService subjectService;
    private final TransactionService transactionService;
    private final TransactionController transactionController;

    /**
     * Erstellt einen neuen AccountController.
     *
     * @param accountService        der Service für Konten.
     * @param subjectService        der Service für Fächer.
     * @param transactionService    der Service für Transaktionen.
     * @param transactionController der Controller für Transaktionen.
     * @since 1.0
     */
    public AccountController(AccountService accountService, SubjectService subjectService,
                             TransactionService transactionService, TransactionController transactionController) {
        this.accountService = accountService;
        this.subjectService = subjectService;
        this.transactionService = transactionService;
        this.transactionController = transactionController;
    }


    /**
     * Durchsucht das System nach allen existierenden Konten.
     *
     * @return Eine Liste, die alle Konten des Systems enthält.
     * @since 1.0
     */
    @GetMapping
    @Secured("ROLE_ADMIN")
    public ResponseEntity<List<AccountResponseDTO>> findAll() {
        List<Account> results = accountService.findAll();
        return ResponseEntity.ok(results.stream().map(AccountResponseDTO::new).collect(Collectors.toList()));
    }

    /**
     * Durchsucht das System nach allen Konten eines bestimmten Fachs.
     *
     * @param subjectId Die ID des Fachs, dessen Konten gesucht werden.
     * @param authUser  Der Benutzer, der die Anfrage stellt.
     * @return Eine Liste, die alle Konten des gesuchten Fachs enthält.
     * @since 1.0
     */
    @GetMapping(params = "subject_id")
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<List<AccountResponseDTO>> findBySubjectId(@RequestParam("subject_id") Integer subjectId,
                                                                    @AuthenticationPrincipal AuthenticatedUser authUser) {
        // validating if the teacher has permission to access the subject and therefore its accounts
        if (authUser.getUser().getRole() == User.UserRole.ROLE_TEACHER) {
            Subject subject = subjectService.findById(subjectId);
            if (subject != null && !subjectService.hasPermission(authUser.getUser(), subject)) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }

        List<Account> results = accountService.findBySubjectId(subjectId);
        return ResponseEntity.ok(results.stream().map(AccountResponseDTO::new).collect(Collectors.toList()));
    }

    /**
     * Durchsucht das System nach allen Konten eines bestimmten Schülers, unabhängig vom Fach.
     *
     * @param studentId Die ID des Schülers.
     * @param authUser  Der Benutzer, der die Anfrage stellt.
     * @return Eine Liste, die alle Fächer des Schülers enthält.
     * @since 1.0
     */
    @GetMapping(params = "student_id")
    @Secured({"ROLE_ADMIN", "ROLE_STUDENT"})
    public ResponseEntity<List<AccountResponseDTO>> findByStudentId(@RequestParam("student_id") Integer studentId,
                                                                    @AuthenticationPrincipal AuthenticatedUser authUser) {
        if (authUser.getUser().getRole() == User.UserRole.ROLE_STUDENT &&
                !authUser.getUser().getId().equals(studentId)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        List<Account> results = accountService.findByStudentId(studentId);
        return ResponseEntity.ok(results.stream().map(AccountResponseDTO::new).collect(Collectors.toList()));
    }

    /**
     * Durchsucht das System nach einem speziellen Konto.
     *
     * @param id       Die ID des Kontos, nach dem gesucht werden soll.
     * @param authUser Der Benutzer, der die Anfrage stellt.
     * @return Das gesuchte Konto, falls es existiert.
     * @since 1.0
     */
    @GetMapping("/{id}")
    @PermitAll
    public ResponseEntity<AccountTransactionsResponseDTO> findById(@PathVariable Integer id,
                                                                   @AuthenticationPrincipal AuthenticatedUser authUser) {
        Account account = accountService.findById(id);

        if (account == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        if (!accountService.hasPermission(authUser.getUser(), account)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        return ResponseEntity.ok(new AccountTransactionsResponseDTO(account));
    }

    /**
     * Durchsucht das System nach einem speziellen Konto für eine Liste aller Transaktionen, die auf diesem Konto
     * durchgeführt wurden.
     *
     * @param id       Die ID des Kontos.
     * @param authUser Der Benutzer, der die Anfrage stellt.
     * @return Eine Liste mit allen Transaktionen, die auf dem gesuchten Konto durchgeführt wurden.
     * @since 1.0
     */
    @GetMapping("/{id}/transactions")
    @PermitAll
    public ResponseEntity<List<TransactionResponseDTO>> getTransactionsById(@PathVariable Integer id,
                                                                            @AuthenticationPrincipal AuthenticatedUser authUser) {
        Account account = accountService.findById(id);

        if (account == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        if (!accountService.hasPermission(authUser.getUser(), account)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        return transactionController.findByAccountId(id, authUser);
    }

    /**
     * Führt eine Transaktion aus.
     *
     * @param id          Die ID des Kontos, auf dem die Transaktion aufgeführt werden soll.
     * @param transaction Die Transaktion, die durchgeführt werden soll.
     * @param authUser    Der Benutzer, der die Anfrage stellt.
     * @return Die durchgeführte Transaktion.
     * @since 1.0
     */
    @PostMapping("/{id}/transactions")
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<TransactionResponseDTO> executeTransaction(@PathVariable Integer id,
                                                                     @Valid @RequestBody TransactionRequestDTO transaction,
                                                                     @AuthenticationPrincipal AuthenticatedUser authUser) {
        Account account = accountService.findById(id);

        if (account == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        if (!accountService.hasPermission(authUser.getUser(), account)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Transaction transactionEntity = transaction.toEntity();
        transactionEntity.setAccount(account);
        transactionEntity.setInitiator(authUser.getUser());

        Transaction result = transactionService.createTransaction(transactionEntity);
        return ResponseEntity.ok(new TransactionResponseDTO(result));
    }

}
