package edu.kit.informatik.pse.edubank.dto.request;

import edu.kit.informatik.pse.edubank.entity.SchoolClass;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * Ein SchoolClassRequestDTO bündelt die Anfrage an eine Klasse in einem Objekt. Da Klassen ausschließlich durch ihren
 * Namen identfiziert werden, enthält dieses Objekt nur den Namen (name).
 *
 * @version 1.0
 * @author EduBank Team
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SchoolClassRequestDTO {

    @NotBlank(message = "Ein gültiger Name ist erforderlich.")
    private String name;

    /**
     * Die Methode nimmt den Namen der als DTO gespeicherten Klasse und erstellt daraus ein entsprechendes
     * {@link SchoolClass} Objekt.
     *
     * @return Eine Instanz von {@link SchoolClass} die durch das SchoolClassRequestDTO repräsentiert wurce.
     * @since 1.0
     */
    public SchoolClass toEntity() {
        return SchoolClass.builder().name(name).build();
    }
}
