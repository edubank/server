package edu.kit.informatik.pse.edubank.dto.request;

import edu.kit.informatik.pse.edubank.entity.Transaction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Ein TransactionRequestDTO enthält die für eine Transaktion wichtigen Informationen. Das beinhaltet in diesem Fall
 * den Titel bzw. Namen der Transaktion (title) und den Betrag (amount).
 *
 * @version 1.0
 * @author EduBank Team
 */
@Data
@Builder
@AllArgsConstructor
public class TransactionRequestDTO {

    @NotBlank(message = "Der Überschrift darf nicht leer sein.")
    private String title;
    @NotNull(message = "Der Betrag darf nicht leer sein.")
    private Double amount;

    /**
     * Diese Methode erstellt aus den im DTO gespeicherten Daten ein neues Transaktions-Objekt und gibt dieses zurück.
     *
     * @return Ein neues {@link Transaction} Objekt, welches auch die Informationen des DTOs enthält.
     * @since 1.0
     */
    public Transaction toEntity() {
        return Transaction.builder()
                .title(title)
                .amount(amount)
                .build();
    }
}
