package edu.kit.informatik.pse.edubank.service.impl;

import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.Transaction;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.repository.AccountRepository;
import edu.kit.informatik.pse.edubank.repository.TransactionRepository;
import edu.kit.informatik.pse.edubank.repository.UserRepository;
import edu.kit.informatik.pse.edubank.service.TransactionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Eine TransactionServiceImpl setzt die im TransactionService definierten Funktionen um und nutzt hierfür ein
 * TransactionRepository, ein AccountRepository und ein UserRepository.
 *
 * @author EduBank Team
 * @version 1.0
 * @see TransactionService
 * @see TransactionRepository
 * @see AccountRepository
 * @see UserRepository
 */
@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;

    public TransactionServiceImpl(TransactionRepository transactionRepository,
                                  AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public List<Transaction> findAll() {
        return transactionRepository.findAll();
    }

    @Override
    public Transaction findById(Integer id) {
        return transactionRepository.findById(id).orElse(null);
    }

    @Override
    public List<Transaction> findByAccountId(Integer accountId) {
        return transactionRepository.findByAccountId(accountId);
    }

    @Override
    @Transactional
    public Transaction createTransaction(Transaction transaction) {
        Account account = getAccountValidated(transaction);
        account.setBalance(BigDecimal.valueOf(account.getBalance()).add(BigDecimal.valueOf(transaction.getAmount())).doubleValue());
        accountRepository.saveAndFlush(account);
        return transactionRepository.save(transaction);
    }

    @Override
    @Transactional
    public List<Transaction> initiateEvent(String title, List<Integer> accountsIds, Double amount, User initiator) {

        List<Account> accounts = getAccountListValidated(accountsIds, initiator);
        accounts.forEach(account ->
                account.setBalance(BigDecimal.valueOf(account.getBalance()).add(BigDecimal.valueOf(amount)).doubleValue()));

        List<Transaction> transactions = accounts.stream()
                .map(account -> Transaction.builder()
                                            .account(account)
                                            .title(title)
                                            .amount(amount)
                                            .initiator(initiator)
                                            .build())
                .collect(Collectors.toList());

        accountRepository.saveAllAndFlush(accounts);
        return transactionRepository.saveAllAndFlush(transactions);
    }

    private Account getAccountValidated(Transaction transaction) {
        return accountRepository.findById(transaction.getAccount().getId())
            .orElseThrow(() -> {
                throw new ValidationException("account_id", "Fach mit ID " + transaction.getAccount().getId() + " existiert nicht.");
            });
    }

    private List<Account> getAccountListValidated(List<Integer> accountIds, User initiator) {

        List<Account> accounts = accountRepository.findByIdIn(accountIds);
        List<Integer> foundAccountIds = accounts.stream().map(Account::getId).collect(Collectors.toList());

        // validate all accounts with given ids exist
        accountIds.stream()
                .filter(id -> !foundAccountIds.contains(id))
                .findFirst()
                .ifPresent(id -> {
                    throw new ValidationException("account_id", "Konto mit ID " + id + " existiert nicht.");
                });

        // validate the initiator has permission to do transaction
        if (initiator.getRole() == User.UserRole.ROLE_TEACHER) {
            accounts.stream()
                    .filter(account -> !account.getSubject().getTeacher().getId().equals(initiator.getId()))
                    .findFirst()
                    .ifPresent(id -> {
                        throw new ValidationException("account_id", "Konto mit ID " + id +
                                " darf nicht vom Benutzer mit ID " + initiator.getId() + " geändert werden.");
                    });
        }

        return accounts;
    }
}
