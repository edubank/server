package edu.kit.informatik.pse.edubank.service.impl;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Ein ValidationException repräsentiert einen Fehler, der während der Bearbeitung einer Anfrage auftritt.
 *
 * @author EduBank Team
 * @version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class ValidationException extends RuntimeException {

    private final String fieldName;
    private final String message;
}
