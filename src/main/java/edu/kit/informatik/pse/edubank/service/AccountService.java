package edu.kit.informatik.pse.edubank.service;

import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.User;

import java.util.List;

/**
 * Ein AccountService stellt Funktionen für das Arbeiten mit Konten zur Verfügung.
 *
 * @author EduBank Team
 * @version 1.0
 */
public interface AccountService {

    /**
     * Durchsucht das System nach allen Konten.
     *
     * @return Eine Liste mit allen Konten, die im System existieren.
     * @since 1.0
     */
    List<Account> findAll();

    /**
     * Durchsucht das System nach einem speziellen Konto.
     *
     * @param id Die ID des Kontos, das gesucht wird.
     * @return Das gesuchte Konto, falls es existiert.
     * @since 1.0
     */
    Account findById(Integer id);

    /**
     * Durchsucht das System nach allen Konten eines speziellen Schülers.
     *
     * @param studentId Die ID des Schülers, nach dessen Konten gesucht werden soll.
     * @return Eine Liste mit allen Konten des Schülers, falls der Schüler existiert und Konten hat.
     * @since 1.0
     */
    List<Account> findByStudentId(Integer studentId);

    /**
     * Durchsucht das System nach allen Konten eines speziellen Fachs.
     *
     * @param subjectId Die ID des Fachs, dessen Konten gesucht werden sollen.
     * @return Eine Liste mit allen Konten des Fachs, falls das Fach existiert und Konten enthält.
     * @since 1.0
     */
    List<Account> findBySubjectId(Integer subjectId);

    /**
     * Überprüft, ob ein Benutzer Zugriff auf ein Konto hat.
     *
     * @param user Der Benutzer, für den die Validation durchgeführt werden sollte.
     * @param account Das Konto, für das die Validation durchgeführt werden sollte.
     * @return 'true' falls der Benutzer Zugriff hat.
     * @since 1.0
     */
    boolean hasPermission(User user, Account account);

}
