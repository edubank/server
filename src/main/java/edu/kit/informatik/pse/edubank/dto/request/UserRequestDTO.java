package edu.kit.informatik.pse.edubank.dto.request;

import edu.kit.informatik.pse.edubank.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Ein UserRequestDTO bündelt alle bei einer Benutzeranfrage relevanten Informationen über diesen Benutzer. Das umfasst
 * die Rolle (role), den Vornamen (firstName), den Nachnamen (lastName) und die E-Mail-Adresse (email).
 *
 * @author EduBank Team
 * @version 1.0
 */
@Data
@Builder
@AllArgsConstructor
public class UserRequestDTO {

    @NotNull(message = "Eine gültige Rolle ist erforderlich.")
    private User.UserRole role;
    @NotBlank(message="Ein gültiger Vorname ist erforderlich.")
    private String firstName;
    @NotBlank(message="Ein gültiger Nachname ist erforderlich.")
    private String lastName;
    @Email(message = "Eine gültige E-Mail-Adresse ist erforderlich.")
    private String email;

    /**
     * Diese Methode nimmt die im DTO gespeicherten Daten und erstellt damit einen äquivalenten Benutzer (User).
     *
     * @return Eine neue Instanz von {@link User} mit den gleichen Informationen wie im DTO.
     * @since 1.0
     */
    public User toEntity() {
        return User.builder()
                .role(role)
                .firstName(firstName)
                .lastName(lastName)
                .email(email != null && !email.isBlank() ? email : null)
                .build();
    }
}
