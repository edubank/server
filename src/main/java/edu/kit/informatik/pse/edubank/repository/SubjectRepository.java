package edu.kit.informatik.pse.edubank.repository;

import edu.kit.informatik.pse.edubank.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Ein SubjectRepository bündelt alle Fächer des Systems und bietet Funktionen, nach Gruppen von Fächern zu suchen.
 *
 * @version 1.0
 * @author EduBank Team
 */
public interface SubjectRepository extends JpaRepository<Subject, Integer> {

    /**
     * Diese Methode durchsucht das System nach allen Fächern, die zu der Klasse mit der gegebenen ID gehören und gibt
     * diese zurück.
     *
     * @param classId Die ID der Klasse, deren Fächer gesucht sind.
     * @return Eine Liste mit allen Fächern der Klasse.
     * @since 1.0
     */
    List<Subject> findBySchoolClassId(Integer classId);

    /**
     * Diese Methode durchsucht das System nach allen Fächern, die zu dem Lehrer mit der gegebenen ID gehören und gibt
     * diese zurück.
     *
     * @param teacherId Die ID des Lehrers, deren Fächer gesucht sind.
     * @return Eine Liste mit allen Fächern vom Lehrer.
     * @since 1.0
     */
    List<Subject> findByTeacherId(Integer teacherId);

}
