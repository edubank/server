package edu.kit.informatik.pse.edubank.service;

import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.User;

import java.util.List;

/**
 * Ein SubjectService stellt Funktionen für das Arbeiten mit Fächern zur Verfügung.
 *
 * @author EduBank Team
 * @version 1.0
 */
public interface SubjectService {

    /**
     * Durchsucht das System nach allen Fächern.
     *
     * @return Eine Liste mit allen Fächern im System.
     * @since 1.0
     */
    List<Subject> findAll();

    /**
     * Durchsucht das System nach einem speziellen Fach.
     *
     * @param id Die ID des Fachs, die gesucht werden soll.
     * @return Das Fach mit der gegeben ID, falls es im System existiert.
     * @since 1.0
     */
    Subject findById(Integer id);

    /**
     * Durchsucht das System nach allen Fächern einer Klasse.
     *
     * @param classId Die ID der Klasse, deren Fächer gesucht werden sollen.
     * @return Eine Liste mit allen Fächern, die zur Klassen mit der gegeben ID gehören.
     * @since 1.0
     */
    List<Subject> findByClassId(Integer classId);

    /**
     * Durchsucht das System nach allen Fächern eines Lehrers.
     *
     * @param teacherId Die ID des Lehrers, deren Fächer gesucht werden sollen.
     * @return Eine Liste mit allen Fächern, die zum Lehrer mit der gegebenen ID gehören.
     * @since 1.0
     */
    List<Subject> findByTeacherId(Integer teacherId);

    /**
     * Legt ein neues Fach an.
     *
     * @param subject Das Fach, das dem System hinzugefügt werden soll.
     * @return Das neu hinzugefügte Fach.
     * @since 1.0
     */
    Subject createSubject(Subject subject);

    /**
     * Nimmt Änderungen an einem bestehenden Fach vor.
     *
     * @param id      Die ID des Fachs, das geändert werden soll.
     * @param subject Das geänderte Fach.
     * @return Das neue, geänderte Fach im System.
     * @since 1.0
     */
    Subject updateSubject(Integer id, Subject subject);

    /**
     * Fügt eine Gruppe von Schülern einem Fach hinzu.
     *
     * @param subjectId  Die ID des Fachs.
     * @param studentIds Die IDs der Schüler.
     * @return Eine Liste mit allen neu angelegten Konten der Schüler.
     * @since 1.0
     */
    List<Account> addStudentsToSubject(Integer subjectId, List<Integer> studentIds);

    /**
     * Entfernt eine Gruppe von Schülern aus einem Fach.
     *
     * @param subjectId  Die ID des Fachs.
     * @param studentIds Die IDs der Schüler.
     * @return Eine Liste der Konten, die aus dem System entfernt wurden.
     * @since 1.0
     */
    List<Account> removeStudentsFromSubject(Integer subjectId, List<Integer> studentIds);

    /**
     * Löscht ein Fach.
     *
     * @param id Die ID des Fachs, das gelöscht werden soll.
     * @return 'true' falls das Fach gelöscht wurde, sonst 'false'.
     * @since 1.0
     */
    boolean deleteSubjectById(Integer id);

    /**
     * Überprüft, ob ein Benutzer Zugriff auf ein Fach hat.
     *
     * @param user Der Benutzer, für den die Validation durchgeführt werden sollte.
     * @param subject Das Fach, für das die Validation durchgeführt werden sollte.
     * @return 'true' der Benutzer Zugriff hat.
     * @since 1.0
     */
    boolean hasPermission(User user, Subject subject);
}
