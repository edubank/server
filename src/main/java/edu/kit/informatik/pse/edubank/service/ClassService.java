package edu.kit.informatik.pse.edubank.service;

import edu.kit.informatik.pse.edubank.entity.SchoolClass;

import java.util.List;

/**
 * Ein ClassService stellt Funktionen für das Arbeiten mit Klassen zur Verfügung.
 *
 * @author EduBank Team
 * @version 1.0
 */
public interface ClassService {

    /**
     * Durchsucht das System nach allen Klassen.
     *
     * @return Eine Liste mit allen existierenden Klassen.
     * @since 1.0
     */
    List<SchoolClass> findAll();

    /**
     * Durchsucht das System nach einer speziellen Klasse.
     *
     * @param id Die ID der gesuchten Klasse.
     * @return Die Klasse mit der gegebenen ID, falls diese im System existiert.
     * @since 1.0
     */
    SchoolClass findById(Integer id);

    /**
     * Legt eine neue Klasse im System an.
     *
     * @param schoolClass Die Klasse, die dem System hinzugefügt werden soll.
     * @return Die neu hinzugefügte Klasse.
     * @since 1.0
     */
    SchoolClass createClass(SchoolClass schoolClass);

    /**
     * Nimmt Änderungen an einer Klasse vor.
     *
     * @param id          Die ID der Klasse, die geändert werden soll.
     * @param schoolClass Die geänderte Klasse.
     * @return Die neue, geänderte Klasse, die jetzt im System ist.
     * @since 1.0
     */
    SchoolClass updateClass(Integer id, SchoolClass schoolClass);

    /**
     * Löscht eine Klasse aus dem System.
     *
     * @param id Die ID der Klasse, die gelöscht werden soll.
     * @return 'true' wenn die Klasse erfolgreich gelöscht wurde, sonst 'false'.
     * @since 1.0
     */
    boolean deleteClassById(Integer id);
}
