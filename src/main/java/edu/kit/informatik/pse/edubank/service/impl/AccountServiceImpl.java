package edu.kit.informatik.pse.edubank.service.impl;

import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.repository.AccountRepository;
import edu.kit.informatik.pse.edubank.service.AccountService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Eine AccountServiceImpl setzt die im AccountService definierten Funktionen um und nutzt hierfür ein
 * AccountRepository.
 *
 * @author EduBank Team
 * @version 1.0
 * @see AccountService
 * @see AccountRepository
 */
@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    @Override
    public Account findById(Integer id) {
        return accountRepository.findById(id).orElse(null);
    }

    @Override
    public List<Account> findByStudentId(Integer studentId) {
        return accountRepository.findByStudentId(studentId);
    }

    @Override
    public List<Account> findBySubjectId(Integer subjectId) {
        return accountRepository.findBySubjectId(subjectId);
    }

    @Override
    public boolean hasPermission(User user, Account account) {
        switch (user.getRole()) {
            case ROLE_STUDENT:
                return account.getStudent().getId().equals(user.getId());
            case ROLE_TEACHER:
                return account.getSubject().getTeacher().getId().equals(user.getId());
            case ROLE_ADMIN:
                return true;
            default:
                return false;
        }
    }

}
