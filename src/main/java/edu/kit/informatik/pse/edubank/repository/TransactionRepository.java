package edu.kit.informatik.pse.edubank.repository;

import edu.kit.informatik.pse.edubank.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Ein TransactionRepository bündelt alle Transaktionen des Systems und bietet Funktionen, diese zu filtern.
 *
 * @version 1.0
 * @author EduBank Team
 */
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

    /**
     * Diese Methode bietet die Möglichkeit, alle Transaktionen, die zu einem gegeben Konto gehören, zu bekommen.
     *
     * @param accountId Die ID des Kontos, an dem man interessiert ist.
     * @return Eine Liste aller Transaktionen, die auf diesem Konto durchgeführt wurden.
     * @since 1.0
     */
    List<Transaction> findByAccountId(Integer accountId);
}
