package edu.kit.informatik.pse.edubank.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * Eine Transaktion (Transaction) wird immer auf einem Konto (account) von einem Initator (initiator), einem Lehrer oder
 * Administrator, durchgeführt. Außerdem hat jede Transaktion zusätzlich zu einem Namen (title) und dem Betrag (amount)
 * noch eine eindeutige ID (id) und ein Erstellungsatum (createdAt).
 *
 * @version 1.0
 * @author EduBank Team
 */
@Data
@Builder
@Entity
@Table(name = "transactions")
@EntityListeners(AuditingEntityListener.class)
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;
    private String title;
    private Double amount;
    @ManyToOne
    @JoinColumn(name = "initiator_id")
    private User initiator;
    @CreatedDate
    private Date createdAt;

}
