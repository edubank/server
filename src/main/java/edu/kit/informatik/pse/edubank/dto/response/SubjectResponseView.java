package edu.kit.informatik.pse.edubank.dto.response;

import edu.kit.informatik.pse.edubank.entity.Subject;
import lombok.Data;

/**
 * Eine SubjectResponseView bündelt Informationen für Antworten bei Fach-Anfragen. Hierzu zählen die ID des Fachs (id),
 * der Name des Fachs (name) und der verwaltende Lehrer (teacher).
 *
 * @author EduBank Team
 * @version 1.0
 * @see SubjectResponseDTO
 * @see SubjectClassResponseView
 */
@Data
public class SubjectResponseView {
    private Integer id;
    private String name;
    private UserResponseDTO teacher;

    /**
     * Erzeugt eine SubjectResponseView.
     *
     * @param subject Das Fach, dessen Informationen gebündelt werden.
     * @since 1.0
     */
    public SubjectResponseView(Subject subject) {
        id = subject.getId();
        name = subject.getName();
        teacher = subject.getTeacher() == null ? null : new UserResponseDTO(subject.getTeacher());
    }
}