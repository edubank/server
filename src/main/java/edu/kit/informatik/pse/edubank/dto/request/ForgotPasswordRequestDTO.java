package edu.kit.informatik.pse.edubank.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * Ein ForgotPasswordRequestDTO bündelt alle Daten, die für Anfragen bei einem vergessenen Passworts wichtig sind. In
 * diesem Fall ist damit ausschließlich die E-Mail-Adresse (email) gemeint, da das neu generierte Passwort an diese
 * geschickt
 * wird.
 *
 * @version 1.0
 * @author EduBank Team
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ForgotPasswordRequestDTO {

    @NotBlank(message = "Eine E-Mail-Adresse ist erforderlich.")
    @Email(message = "Eine gültige E-Mail-Adresse ist erforderlich.")
    private String email;
}
