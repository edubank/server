package edu.kit.informatik.pse.edubank.dto.response;

import edu.kit.informatik.pse.edubank.entity.Transaction;
import lombok.Data;

import java.util.Date;

/**
 * Ein TransactionResponseDTO bündelt die gleichen Informationen wie eine TransactionResponseView und zusätzlich noch
 * das Konto, auf dem die Transaktion durchgeführt wurde (account).
 *
 * @author EduBank Team
 * @version 1.0
 * @see TransactionResponseView
 */
@Data
public class TransactionResponseDTO {
    private Integer id;
    private AccountResponseDTO account;
    private String title;
    private Double amount;
    private UserResponseDTO initiator;
    private Date createdAt;

    /**
     * Erzeugt ein neues TransactionResponseDTO.
     *
     * @param transaction Die Transaktion, deren Informationen gebündelt werden.
     */
    public TransactionResponseDTO(Transaction transaction) {
        id = transaction.getId();
        account = new AccountResponseDTO(transaction.getAccount());
        title = transaction.getTitle();
        amount = transaction.getAmount();
        initiator = transaction.getInitiator() == null ? null : new UserResponseDTO(transaction.getInitiator());
        createdAt = transaction.getCreatedAt();
    }
}
