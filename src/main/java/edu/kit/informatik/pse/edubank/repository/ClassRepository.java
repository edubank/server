package edu.kit.informatik.pse.edubank.repository;

import edu.kit.informatik.pse.edubank.entity.SchoolClass;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Ein ClassRepository bündelt alle Klassen innerhalb des Systems. Da Klassen ausschließlich als organisatorische
 * Einheit genutzt werden, gibt es hier keine Filter, da diese nicht nötig sind.
 *
 * @version 1.0
 * @author EduBank Team
 */
public interface ClassRepository extends JpaRepository<SchoolClass, Integer> {
}
