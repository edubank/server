package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.request.ProfileDataRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.UserResponseDTO;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;

/**
 * Ein UserAccountController verwaltet alle Anfragen und Zugriffe auf Benutzerkonten. Alle Rückgaben der Methoden werden
 * in einem ResponseEntity verpackt.
 *
 * @author EduBank Team
 * @version 1.0
 */
@RestController
@RequestMapping("/me")
public class UserAccountController {

    private final UserService userService;

    public UserAccountController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Durchsucht das System nach dem Benutzerkonto des anfragenden Benutzers.
     *
     * @param authUser Der Benutzer, der die Anfrage stellt.
     * @return Das entsprechende Benutzerkonto.
     * @since 1.0
     */
    @GetMapping
    @PermitAll
    public ResponseEntity<UserResponseDTO> getMyUserAccount(@AuthenticationPrincipal AuthenticatedUser authUser) {
        return ResponseEntity.ok(new UserResponseDTO(authUser.getUser()));
    }

    /**
     * Nimmt Änderungen am Benutzerkonto des anfragenden Benutzers vor.
     *
     * @param userMe   Das geänderte Benutzerkonto.
     * @param authUser Der Benutzer, der die Anfrage stellt.
     * @return Eine Bestätigung für die Änderung.
     * @since 1.0
     */
    @PutMapping
    @PermitAll
    public ResponseEntity<UserResponseDTO> updateMyUserAccount(@Valid @RequestBody ProfileDataRequestDTO userMe,
                                                               @AuthenticationPrincipal AuthenticatedUser authUser) {
        authUser.getUser().setPassword("");

        if (userMe.getPassword() != null && !userMe.getPassword().isEmpty()) {
            authUser.getUser().setPassword(userMe.getPassword());
        }

        if (userMe.getEmail() != null && !userMe.getEmail().isEmpty()) {
            authUser.getUser().setEmail(userMe.getEmail());
        }

        User result = userService.updateUser(authUser.getUser().getId(), authUser.getUser());
        return ResponseEntity.ok(new UserResponseDTO(result));
    }
}
