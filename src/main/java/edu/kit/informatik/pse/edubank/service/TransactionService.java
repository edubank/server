package edu.kit.informatik.pse.edubank.service;

import edu.kit.informatik.pse.edubank.entity.Transaction;
import edu.kit.informatik.pse.edubank.entity.User;

import java.util.List;

/**
 * Ein TransactionService stellt Funktionen für das Arbeiten mit Transaktionen zur Verfügung.
 *
 * @author EduBank Team
 * @version 1.0
 */
public interface TransactionService {

    /**
     * Durchsucht das System nach allen Transaktionen.
     *
     * @return Eine Liste mit allen Transaktionen, die im System existieren.
     * @since 1.0
     */
    List<Transaction> findAll();

    /**
     * Durchsucht das System nach einer speziellen Transaktion.
     *
     * @param id Die ID der gesuchten Transaktion.
     * @return Die gesuchte Transaktion, falls sie im System existiert.
     * @since 1.0
     */
    Transaction findById(Integer id);

    /**
     * Durchsucht das System nach allen Transaktionen, die zu einem gegebenen Konto gehören.
     *
     * @param accountId Die ID des Kontos.
     * @return Eine Liste aller Transaktionen zu diesem Konto.
     * @since 1.0
     */
    List<Transaction> findByAccountId(Integer accountId);

    /**
     * Legt eine neue Transaktion im System an.
     *
     * @param transaction Die anzulegende Transaktion.
     * @return Die neu angelegte Transaktion.
     * @since 1.0
     */
    Transaction createTransaction(Transaction transaction);

    /**
     * Legt ein neues Event an.
     *
     * @param title       Der Titel des Events.
     * @param accountsIds Die IDs der Konten, die das Event betrifft.
     * @param amount      Der Betrag des Events.
     * @param initiator   Der Benutzer, der das Event initiiert hat.
     * @return Eine Liste aller Transaktionen, die aus dem Event resultieren.
     * @since 1.0
     */
    List<Transaction> initiateEvent(String title, List<Integer> accountsIds, Double amount, User initiator);
}
