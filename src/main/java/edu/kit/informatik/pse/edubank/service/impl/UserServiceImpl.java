package edu.kit.informatik.pse.edubank.service.impl;

import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.repository.AccountRepository;
import edu.kit.informatik.pse.edubank.repository.SubjectRepository;
import edu.kit.informatik.pse.edubank.repository.TransactionRepository;
import edu.kit.informatik.pse.edubank.repository.UserRepository;
import edu.kit.informatik.pse.edubank.service.TransactionService;
import edu.kit.informatik.pse.edubank.service.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Eine UserServiceImpl setzt die im UserService definierten Funktionen um und nutzt hierfür ein TransactionRepository,
 * ein AccountRepository und ein UserRepository.
 *
 * @author EduBank Team
 * @version 1.0
 * @see TransactionService
 * @see TransactionRepository
 * @see AccountRepository
 * @see UserRepository
 */
@Service
public class UserServiceImpl implements UserService {

    public static final Integer GENERATED_PASSWORD_LENGTH = 10;

    private final UserRepository userRepository;
    private final AccountRepository accountRepository;
    private final SubjectRepository subjectRepository;
    private final JavaMailSender emailSender;

    public UserServiceImpl(UserRepository userRepository, AccountRepository accountRepository,
                           SubjectRepository subjectRepository, JavaMailSender emailSender) {
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
        this.subjectRepository = subjectRepository;
        this.emailSender = emailSender;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(Integer id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public List<User> findBySubjectId(Integer subjectId) {
        return userRepository.findBySubjects_Id(subjectId);
    }

    @Override
    public List<User> findByRole(User.UserRole role) {
        return userRepository.findByRole(role);
    }

    @Override
    public User createUser(User user) {
        // validate email
        if (user.getEmail() != null && !user.getEmail().isEmpty()) {
            userRepository.findByEmail(user.getEmail()).ifPresent(userFound -> {
                throw new ValidationException("email", "Es gibt schon einen Benutzer mit dieser E-Mail-Adresse.");
            });
        }

        // random username generation
        String username;
        do {
            username = user.getRole().name().replace("ROLE_", "").toLowerCase().charAt(0)
                    + RandomStringUtils.randomAlphabetic(4).toLowerCase();
        } while (userRepository.findByUsername(username).isPresent());
        user.setUsername(username);

        String password;
        // random password generation
        if (user.getPassword() == null || user.getPassword().isEmpty()) {
            String passwordGen = generatePassword();
            user.setPasswordGen(passwordGen);
            password = passwordGen;
        } else {
            password = user.getPassword();
        }

        user.setPassword(new BCryptPasswordEncoder().encode(password));

        return userRepository.saveAndFlush(user);
    }

    @Override
    public User updateUser(Integer id, User user) {
        User existingUser = userRepository.findById(id)
                .orElseThrow(() -> new ValidationException("id", "Benutzer mit ID " + id + " existiert nicht."));

        user.setId(id);
        user.setUsername(existingUser.getUsername());

        if (user.getEmail() != null && !user.getEmail().isEmpty()) {
            userRepository.findByEmail(user.getEmail()).ifPresent(userFound -> {

                if (!userFound.getId().equals(id)) {
                    throw new ValidationException("email", "Es gibt schon einen Benutzer mit dieser E-Mail-Adresse.");
                }
            });
        } else {
            user.setEmail(existingUser.getEmail());
        }

        if (existingUser.getRole() == User.UserRole.ROLE_STUDENT &&
            user.getRole() != User.UserRole.ROLE_STUDENT) {
            throw new ValidationException("role","Die Rolle von einem Schüler darf nicht geändert werden.");
        }

        if ((existingUser.getRole() == User.UserRole.ROLE_ADMIN ||
            existingUser.getRole() == User.UserRole.ROLE_TEACHER) &&
            user.getRole() == User.UserRole.ROLE_STUDENT) {
            throw new ValidationException("role", "Die Benutzer mit dieser Rolle dürfen nicht zur Rolle eines Schülers geändert werden.");
        }

        if (user.getPassword() != null && !user.getPassword().isEmpty()) {
            if (new BCryptPasswordEncoder().matches(user.getPassword(), existingUser.getPassword())) {
                throw new ValidationException("password", "Das neue Passwort darf nicht das gleiche sein.");
            }
            user.setPasswordGen(null);
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        } else {
            user.setPasswordGen(existingUser.getPasswordGen());
            user.setPassword(existingUser.getPassword());
        }

        return userRepository.saveAndFlush(user);
    }

    @Override
    @Transactional
    public boolean deleteUserById(Integer id) {

        Optional<User> user = userRepository.findById(id);

        if (user.isEmpty()) {
            return false;
        }

        if (user.get().getRole() == User.UserRole.ROLE_ADMIN &&
            userRepository.findByRole(User.UserRole.ROLE_ADMIN).size() == 1) {
            throw new ValidationException("id", "Dieser Administrator darf nicht gelöscht werden, " +
                    "im System soll es mindestens einen Administrator geben.");
        }

        if (user.get().getRole() == User.UserRole.ROLE_TEACHER) {
            List<Subject> subjects = subjectRepository.findByTeacherId(user.get().getId());
            if (!subjects.isEmpty()) {
                throw new ValidationException("id", "Dieser Lehrer darf nicht gelöscht werden, da er immer noch Fächer verwaltet.");
            }
        }

        if (user.get().getRole() == User.UserRole.ROLE_STUDENT) {
            accountRepository.deleteAll(user.get().getAccounts());
            accountRepository.flush();
            accountRepository.flush();
        }

        try {
            userRepository.delete(user.get());
        } catch (Exception e) {
            userRepository.delete(user.get());
        }
        return true;
    }

    @Override
    public void resetPassword(String email) {
        User existingUser = userRepository.findByEmail(email)
                .orElseThrow(() -> new ValidationException("email", "Benutzer mit E-Mail-Adresse " + email + " existiert nicht."));

        String newPassword = generatePassword();
        existingUser.setPasswordGen(newPassword);
        existingUser.setPassword(new BCryptPasswordEncoder().encode(newPassword));

        userRepository.save(existingUser);

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("edubank.noreply@gmail.com");
        message.setTo(email);
        message.setSubject("EduBank - Passwort zurücksetzen");
        message.setText("Neues Passwort für " + existingUser.getFirstName() + " " + existingUser.getLastName() +
                ": " + newPassword);
        emailSender.send(message);
    }

    private String generatePassword() {
        return RandomStringUtils.randomAlphanumeric(GENERATED_PASSWORD_LENGTH).toLowerCase();
    }

}
