package edu.kit.informatik.pse.edubank.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * Ein Fach (Subject) ist die primäre Einheit, die innerhalb eines Systems verwendet wird. Jedes Fach ist durch eine
 * eindeutige ID (id) identifizierbar. Zusätzlich zur ID hat jedes Fach einen Namen (name) und einen Verweis auf die
 * Klasse (SchoolClass), der sie zugeordnet ist, sowie auf den Lehrer (teacher), der die Klasse verwaltet. Hinzu kommt
 * noch eine Liste aller Konten (accounts), die zu dem Fach gehören.
 *
 * @version 1.0
 * @author EduBank Team
 */
@Data
@Builder
@Entity
@Table(name = "subjects")
@AllArgsConstructor
@NoArgsConstructor
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @ManyToOne(targetEntity = SchoolClass.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "class_id")
    private SchoolClass schoolClass;
    @ManyToOne
    @JoinColumn(name = "teacher_id")
    private User teacher;
    @Singular
    @OneToMany(mappedBy = "subject", cascade=CascadeType.REMOVE)
    private List<Account> accounts;
}
