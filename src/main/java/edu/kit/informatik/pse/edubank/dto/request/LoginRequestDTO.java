package edu.kit.informatik.pse.edubank.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Ein LoginRequestDTO enthält die beiden, für eine Anmelde-Anfrage relevanten Informationen: den Benutzernamen - auch
 * als Kürzel bezeichnet - (username) und das gewählte oder zugewiesene Passwort (password).
 *
 * @version 1.0
 * @author EduBank Team
 */
@Data
@Builder
@AllArgsConstructor
public class LoginRequestDTO {

    @NotBlank(message = "Ein gültiger Benutzername ist erforderlich.")
    private String username;
    @NotBlank(message = "Ein gültiges Passwort ist erforderlich.")
    private String password;

}
