package edu.kit.informatik.pse.edubank.service.impl;

import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.SchoolClass;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.repository.AccountRepository;
import edu.kit.informatik.pse.edubank.repository.ClassRepository;
import edu.kit.informatik.pse.edubank.repository.SubjectRepository;
import edu.kit.informatik.pse.edubank.repository.UserRepository;
import edu.kit.informatik.pse.edubank.service.SubjectService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Eine SubjectServiceImpl setzt die im ClassService definierten Funktionen um und nutzt hierfür ein SubjectRepository,
 * ein ClassRepository, ein AccountRepository und ein UserRepository.
 *
 * @author EduBank Team
 * @version 1.0
 * @see edu.kit.informatik.pse.edubank.service.ClassService
 * @see SubjectRepository
 * @see ClassRepository
 * @see AccountRepository
 * @see UserRepository
 */
@Service
public class SubjectServiceImpl implements SubjectService {

    private final SubjectRepository subjectRepository;
    private final ClassRepository classRepository;
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;

    public SubjectServiceImpl(SubjectRepository subjectRepository, ClassRepository classRepository,
                              AccountRepository accountRepository, UserRepository userRepository) {
        this.subjectRepository = subjectRepository;
        this.classRepository = classRepository;
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Subject> findAll() {
        return subjectRepository.findAll();
    }

    @Override
    public Subject findById(Integer id) {
        return subjectRepository.findById(id).orElse(null);
    }

    @Override
    public List<Subject> findByClassId(Integer classId) {
        return subjectRepository.findBySchoolClassId(classId);
    }

    @Override
    public List<Subject> findByTeacherId(Integer teacherId) {
        return subjectRepository.findByTeacherId(teacherId);
    }

    @Override
    public Subject createSubject(Subject subject) {
        validate(subject);
        return subjectRepository.save(subject);
    }

    @Override
    public Subject updateSubject(Integer id, Subject subject) {
        if (!subjectRepository.existsById(id)) {
            throw new ValidationException("id", "Fach mit ID " + id + " existiert nicht.");
        }

        subject.setId(id);
        validate(subject);
        return subjectRepository.save(subject);
    }

    @Override
    public List<Account> addStudentsToSubject(Integer subjectId, List<Integer> studentIds) {

        // validate if subject with the given id exists
        Subject subject = subjectRepository.findById(subjectId)
                .orElseThrow(() -> new ValidationException("subject_id", "Fach mit ID " + subjectId + " existiert nicht."));

        // get the students with the given ids
        List<User> students = getStudentListValidated(studentIds);

        // validate that none of the given student ids are already in the subject
        subject.getAccounts()
            .stream()
            .parallel()
            .map(Account::getStudent)
            .map(User::getId)
            .filter(studentIds::contains)
            .findAny()
            .ifPresent(existingId -> {
                throw new ValidationException("student_ids", "Schüler mit ID " + existingId + " ist schon im Fach.");
            });

        // add student accounts for the subject
        List<Account> newAccounts = students.stream()
                .map(user -> Account.builder()
                                    .subject(subject)
                                    .student(user)
                                    .build())
                .collect(Collectors.toList());

        accountRepository.saveAllAndFlush(newAccounts);

        return accountRepository.findBySubjectId(subjectId);
    }

    @Override
    @Transactional
    public List<Account> removeStudentsFromSubject(Integer subjectId, List<Integer> studentIds) {

        // validate if subject with the given id exists
        Subject subject = subjectRepository.findById(subjectId)
                .orElseThrow(() -> new ValidationException("subject_id", "Fach mit ID " + subjectId + " existiert nicht."));

        // validate the students with the given ids
        getStudentListValidated(studentIds);

        // validate that all of the given student ids are already in the subject
        List<Integer> existingUserIds = subject.getAccounts()
                .stream()
                .parallel()
                .map(Account::getStudent)
                .map(User::getId)
                .collect(Collectors.toList());

        studentIds.stream()
                .filter(studentId -> !existingUserIds.contains(studentId))
                .findFirst()
                .ifPresent(studentId -> {
                    throw new ValidationException("students_id", "Schüler mit ID " + studentId + " ist nicht im Fach.");
                });

        // delete accounts of students in the subject
        accountRepository.deleteBySubjectIdAndStudentIdIn(subjectId, studentIds);

        return accountRepository.findBySubjectId(subjectId);
    }

    @Override
    public boolean deleteSubjectById(Integer id) {

        if (subjectRepository.findById(id).isPresent()) {
            subjectRepository.deleteById(id);
            return true;
        }

        return false;
    }

    @Override
    public boolean hasPermission(User user, Subject subject) {
        switch (user.getRole()) {
            case ROLE_TEACHER:
                return subject.getTeacher().getId().equals(user.getId());
            case ROLE_ADMIN:
                return true;
            default:
                return false;
        }
    }

    private void validate(Subject subject) {
        if (subject.getSchoolClass() != null) {
            SchoolClass schoolClass = classRepository.findById(subject.getSchoolClass().getId())
                    .orElseThrow(() -> new ValidationException("class_id", "Klasse mit ID " + subject.getSchoolClass().getId() + " existiert nicht."));
            subject.setSchoolClass(schoolClass);
        }

        User teacher = userRepository.findById(subject.getTeacher().getId())
                .orElseThrow(() -> new ValidationException("teacher_id", "Benutzer mit ID " + subject.getTeacher().getId() + " existiert nicht."));

        if (teacher.getRole() != User.UserRole.ROLE_TEACHER) {
            throw new ValidationException("teacher_id", "Benutzer mit ID " + subject.getTeacher().getId() + " ist kein Lehrer.");
        }

        subject.setTeacher(teacher);
    }

    private List<User> getStudentListValidated(List<Integer> studentIds) {

        List<User> users = userRepository.findByIdIn(studentIds);
        List<Integer> userIds = users.stream().map(User::getId).collect(Collectors.toList());

        // validate all students with given ids exist
        studentIds.stream()
                .filter(id -> !userIds.contains(id))
                .findFirst()
                .ifPresent(id -> {
                    throw new ValidationException("student_ids", "Benutzer mit ID " + id + " existiert nicht.");
                });

        // validate all given users are students
        users.stream()
                .filter(user -> user.getRole() != User.UserRole.ROLE_STUDENT)
                .findAny()
                .ifPresent(user -> {
                    throw new ValidationException("student_ids", "Benutzer mit ID " + user.getId() + " ist kein Schüler.");
                });

        return users;
    }

}
