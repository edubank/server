package edu.kit.informatik.pse.edubank.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import edu.kit.informatik.pse.edubank.entity.Subject;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Ein SubjectResponseDTO bündelt die gleichen Informationen wie eine SubjectResponseView und zusätzlich noch die
 * Klasse, zu der das Fach gehört, (schoolClass) und die Konten des Fachs (accounts).
 *
 * @author EduBank Team
 * @version 1.0
 * @see SubjectResponseView
 */
@Data
public class SubjectResponseDTO {
    private Integer id;
    private String name;
    @JsonProperty("class")
    private SchoolClassResponseView schoolClass;
    private UserResponseDTO teacher;
    private List<AccountResponseView> accounts;

    /**
     * Erzeugt eine neue SubjectResponseDTO.
     *
     * @param subject Das Fach, dessen Informationen gebündelt werden.
     * @since 1.0
     */
    public SubjectResponseDTO(Subject subject) {
        id = subject.getId();
        name = subject.getName();
        schoolClass =  subject.getSchoolClass() == null ? null : new SchoolClassResponseView(subject.getSchoolClass());
        teacher = subject.getTeacher() == null ? null :  new UserResponseDTO(subject.getTeacher());
        accounts = subject.getAccounts().stream().map(AccountResponseView::new).collect(Collectors.toList());
    }
}
