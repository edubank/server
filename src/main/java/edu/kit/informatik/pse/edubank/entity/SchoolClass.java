package edu.kit.informatik.pse.edubank.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * Eine Klasse (SchoolClass) ist im Grunde eine rein organisatorische Einheit. Jede Klasse hat eine eindeutige ID (id),
 * einen Namen (name) und eine Liste aller Fächer (subjects), welche dieser Klasse untergeordnet sind.
 *
 * @version 1.0
 * @author EduBank Team
 */
@Data
@Builder
@Entity
@Table(name = "classes")
@AllArgsConstructor
@NoArgsConstructor
public class SchoolClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @Singular
    @OneToMany(mappedBy = "schoolClass")
    private List<Subject> subjects;
}
