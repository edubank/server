package edu.kit.informatik.pse.edubank.service.impl;

import edu.kit.informatik.pse.edubank.entity.SchoolClass;
import edu.kit.informatik.pse.edubank.repository.ClassRepository;
import edu.kit.informatik.pse.edubank.service.ClassService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Eine ClassServiceImpl setzt die im ClassService definierten Funktionen um und nutzt hierfür ein ClassRepository.
 *
 * @author EduBank Team
 * @version 1.0
 * @see ClassService
 * @see ClassRepository
 */
@Service
public class ClassServiceImpl implements ClassService {

    private final ClassRepository classRepository;

    public ClassServiceImpl(ClassRepository classRepository) {
        this.classRepository = classRepository;
    }

    @Override
    public List<SchoolClass> findAll() {
        return classRepository.findAll();
    }

    @Override
    public SchoolClass findById(Integer id) {
        return classRepository.findById(id).orElse(null);
    }

    @Override
    public SchoolClass createClass(SchoolClass schoolClass) {
        return classRepository.save(schoolClass);
    }

    @Override
    public SchoolClass updateClass(Integer id, SchoolClass schoolClass) {
        Optional<SchoolClass> existingClass = classRepository.findById(id);

        if (existingClass.isPresent()) {
            schoolClass.setId(id);
            return classRepository.save(schoolClass);
        }

        return null;
    }

    @Override
    public boolean deleteClassById(Integer id) {

        Optional<SchoolClass> schoolClass = classRepository.findById(id);
        if (schoolClass.isPresent()) {

            if (!schoolClass.get().getSubjects().isEmpty()) {
                throw new ValidationException("id", "Diese Klasse enthält Fächer und kann nicht gelöscht werden.");
            }

            classRepository.deleteById(id);
            return true;
        }

        return false;
    }

}
