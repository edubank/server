package edu.kit.informatik.pse.edubank.dto.response;

import edu.kit.informatik.pse.edubank.entity.Transaction;
import lombok.Data;

import java.util.Date;

/**
 * Eine TransactionResponseView bündelt Informationen für Antworten bei Transaktions-Anfragen. Hierzu zählen die ID der
 * Transaktion (id), der Titel der Transaktion (title), der Betrag (amount), der Initiator (initator) und das Datum der
 * Erstellung (date).
 *
 * @author EduBank Team
 * @version 1.0
 * @see TransactionResponseDTO
 */
@Data
public class TransactionResponseView {
    private Integer id;
    private String title;
    private Double amount;
    private UserResponseDTO initiator;
    private Date createdAt;

    /**
     * Erzeugt eine neue TransactionResponseView.
     *
     * @param transaction Die Transaktion, deren Daten gebündelt werden.
     * @since 1.0
     */
    public TransactionResponseView(Transaction transaction) {
        id = transaction.getId();
        title = transaction.getTitle();
        amount = transaction.getAmount();
        initiator = transaction.getInitiator() == null ? null : new UserResponseDTO(transaction.getInitiator());
        createdAt = transaction.getCreatedAt();
    }
}
