package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.service.impl.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * Ein GlobalControllerExceptionHandler soll sich um etwaige Fehler kümmern, die bei unterschiedlichen Controllern
 * auftreten können.
 *
 * @author EduBank Team
 * @version 1.0
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler {

    /**
     * Behandelt Fehler die beim Validierungs-Prozess von Argumenten auftreten.
     *
     * @param ex Die MethodArgumentNotValidException, die geworfen wurde und behandelt werden soll.
     * @return Ein Map mit den Fehlern und zugehörigen Nachrichten.
     * @since 1.0
     */
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleInputValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ValidationException.class)
    public Map<String, String> handleResourceValidationExceptions(ValidationException ex) {
        return Map.of(ex.getFieldName(), ex.getMessage());
    }
}
