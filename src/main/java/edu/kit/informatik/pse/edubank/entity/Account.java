package edu.kit.informatik.pse.edubank.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * Ein Konto (Account) ist immer genau einem Fach und innerhalb dieses Fachs genau einem Schüler zugeordnet. Ein Konto
 * hat neben einer eindeutigen ID (id) und Verweisen auf das Fach (subject) bzw. auf den Schüler (student) noch einen
 * aktuellen Kontostand (balance) und eine Liste aller Transaktionen (transactions), die auf diesem Konto durchgeführt
 * wurden.
 *
 * @version 1.0
 * @author EduBank Team
 */
@Data
@Builder
@Entity
@Table(name = "accounts")
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "subject_id")
    private Subject subject;
    @ManyToOne
    @JoinColumn(name = "student_id")
    private User student;
    @Builder.Default
    private Double balance = 0.0;
    @Singular
    @OneToMany(mappedBy = "account", cascade=CascadeType.REMOVE)
    private List<Transaction> transactions;
}

