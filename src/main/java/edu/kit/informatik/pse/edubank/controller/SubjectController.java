package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.request.SubjectRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.AccountResponseView;
import edu.kit.informatik.pse.edubank.dto.response.SubjectResponseDTO;
import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.service.SubjectService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Ein SubjectController verwaltet alle Anfragen und Zugriffe auf Fächer. Alle Rückgaben der Methoden werden in einem
 * ResponseEntity verpackt.
 *
 * @author EduBank Team
 * @version 1.0
 */
@RestController
@RequestMapping("/subjects")
public class SubjectController {

    private final SubjectService subjectService;

    /**
     * Erstellt einen neuen SubjectController.
     *
     * @param subjectService Der SubjectService, der die benötigten Funktionen zur Verfügung stellt.
     * @since 1.0
     */
    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    /**
     * Durchsucht das System nach allen Fächern.
     *
     * @return Eine Liste mit allen Fächern im System.
     * @since 1.0
     */
    @GetMapping()
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<List<SubjectResponseDTO>> findAll() {
        List<Subject> results = subjectService.findAll();
        return ResponseEntity.ok(results.stream().map(SubjectResponseDTO::new).collect(Collectors.toList()));
    }

    /**
     * Durchsucht das System nach einem speziellen Fach.
     *
     * @param id Die ID des gesuchten Fachs.
     * @param authUser Der angemeldete Benutzer.
     * @return Das Fach, falls es im System existiert.
     */
    @GetMapping("/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<SubjectResponseDTO> findById(@PathVariable Integer id,
                                                       @AuthenticationPrincipal AuthenticatedUser authUser) {
        Subject subject = subjectService.findById(id);

        if (subject == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        if (!subjectService.hasPermission(authUser.getUser(), subject)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        return ResponseEntity.ok(new SubjectResponseDTO(subject));
    }

    /**
     * Durchsucht das System nach allen Fächern einer gegeben Klasse.
     *
     * @param classId Die ID der Klasse.
     * @return Eine Liste mit allen Fächern, die zu der gegebenen Klasse gehören.
     * @since 1.0
     */
    @GetMapping(params = "class_id")
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<List<SubjectResponseDTO>> findByClassId(@RequestParam("class_id") Integer classId) {
        List<Subject> results = subjectService.findByClassId(classId);
        return ResponseEntity.ok(results.stream().map(SubjectResponseDTO::new).collect(Collectors.toList()));
    }

    /**
     * Durchsucht das System nach allen Fächern eines gegebenen Lehrers.
     *
     * @param teacherId Die ID des Lehrers.
     * @return Eine Liste mit allen Fächern, die zu dem gegebenen Lehrer gehören.
     * @since 1.0
     */
    @GetMapping(params = "teacher_id")
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<List<SubjectResponseDTO>> findByTeacherId(@RequestParam("teacher_id") Integer teacherId) {
        List<Subject> results = subjectService.findByTeacherId(teacherId);
        return ResponseEntity.ok(results.stream().map(SubjectResponseDTO::new).collect(Collectors.toList()));
    }

    /**
     * Legt ein neues Fach im System an.
     *
     * @param subject Das Fach, das dem System hinzugefügt werden soll.
     * @param authUser Der angemeldete Benutzer.
     * @return Das neue Fach, das erstellt wurde.
     * @since 1.0
     */
    @PostMapping
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<SubjectResponseDTO> createSubject(@Valid @RequestBody SubjectRequestDTO subject,
                                                            @AuthenticationPrincipal AuthenticatedUser authUser) {
        if (authUser.getUser().getRole() == User.UserRole.ROLE_TEACHER &&
                !subject.getTeacherId().equals(authUser.getUser().getId())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Subject result = subjectService.createSubject(subject.toEntity());
        return ResponseEntity.ok(new SubjectResponseDTO(result));
    }

    /**
     * Nimmt Änderungen an einem existierenden Fach vor.
     *
     * @param id      Die ID des Fachs, das geändert werden soll.
     * @param subject Das Fach, das die Änderungen enthält.
     * @param authUser Der angemeldete Benutzer.
     * @return Das neue, geänderte Fach.
     * @since 1.0
     */
    @PutMapping("/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<SubjectResponseDTO> updateSubject(@PathVariable Integer id,
                                                            @Valid @RequestBody SubjectRequestDTO subject,
                                                            @AuthenticationPrincipal AuthenticatedUser authUser) {
        Subject existingSubject = subjectService.findById(id);

        if (existingSubject == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        if (!subjectService.hasPermission(authUser.getUser(), existingSubject)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        if (authUser.getUser().getRole() == User.UserRole.ROLE_TEACHER &&
                !subject.getTeacherId().equals(authUser.getUser().getId())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        Subject result = subjectService.updateSubject(id, subject.toEntity());
        return ResponseEntity.ok(new SubjectResponseDTO(result));
    }

    /**
     * Fügt Schüler zu einem Fach hinzu.
     *
     * @param subjectId  Die ID des Fachs, dem die Schüler hinzugefügt werden sollen.
     * @param studentIds Eine Liste mit den IDs der Schüler.
     * @param authUser Der angemeldete Benutzer.
     * @return Eine Liste mit den Bestätigungen zu den einzelnen Schülern.
     * @since 1.0
     */
    @PostMapping("/{id}/students")
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<List<AccountResponseView>> addStudentsToSubject(@PathVariable("id") Integer subjectId,
                                                                          @Valid @RequestBody List<Integer> studentIds,
                                                                          @AuthenticationPrincipal AuthenticatedUser authUser) {
        Subject existingSubject = subjectService.findById(subjectId);

        if (existingSubject == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        if (!subjectService.hasPermission(authUser.getUser(), existingSubject)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        List<Account> results = subjectService.addStudentsToSubject(subjectId, studentIds);
        return ResponseEntity.ok(results.stream().map(AccountResponseView::new).collect(Collectors.toList()));
    }

    /**
     * Entfernt Schüler aus einem Fach.
     *
     * @param subjectId  Die ID des Fachs.
     * @param studentIds Eine Liste mit den IDs der Schüler.
     * @param authUser Der angemeldete Benutzer.
     * @return Eine Liste mit den entfernten Konten.
     * @since 1.0
     */
    @DeleteMapping("/{id}/students")
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<List<AccountResponseView>> removeStudentsFromSubject(@PathVariable("id") Integer subjectId,
                                                                               @Valid @RequestBody List<Integer> studentIds,
                                                                               @AuthenticationPrincipal AuthenticatedUser authUser) {
        Subject existingSubject = subjectService.findById(subjectId);

        if (existingSubject == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        if (!subjectService.hasPermission(authUser.getUser(), existingSubject)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        List<Account> results = subjectService.removeStudentsFromSubject(subjectId, studentIds);
        return ResponseEntity.ok(results.stream().map(AccountResponseView::new).collect(Collectors.toList()));
    }

    /**
     * Löscht ein Fach aus dem System.
     *
     * @param id Die ID des Fachs, das gelöscht werden soll.
     * @param authUser Der angemeldete Benutzer.
     * @return 'true', falls das Fach erfolgreich gelöscht wurde, sonst 'false'.
     * @since 1.0
     */
    @DeleteMapping("/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_TEACHER"})
    public ResponseEntity<Boolean> deleteSubject(@PathVariable Integer id,
                                                 @AuthenticationPrincipal AuthenticatedUser authUser) {
        Subject existingSubject = subjectService.findById(id);

        if (existingSubject == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        if (!subjectService.hasPermission(authUser.getUser(), existingSubject)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        return ResponseEntity.ok(subjectService.deleteSubjectById(id));
    }

}
