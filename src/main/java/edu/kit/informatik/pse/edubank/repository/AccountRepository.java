package edu.kit.informatik.pse.edubank.repository;

import edu.kit.informatik.pse.edubank.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Ein AccountRepository enthält alle Konten (Accounts), die innerhalb des Systems existieren und bietet Funktionen,
 * diese zu filtern.
 *
 * @version 1.0
 * @author EduBank Team
 */
public interface AccountRepository extends JpaRepository<Account, Integer> {

    /**
     * Diese Methdode durchsucht das System nach allen Konten, die diesem Schüler zugeordnet sind und gibt diese zurück.
     *
     * @param studentId Die eindeutige ID des Schülers.
     * @return Eine Liste mit allen Konten, die dieser Schüler hat.
     * @since 1.0
     */
    List<Account> findByStudentId(Integer studentId);

    /**
     * Diese Methdode durchsucht das System nach allen Konten, die diesem Fach zugeordnet sind und gibt diese zurück.
     *
     * @param subjectId Die eindeutige ID des Fachs.
     * @return Eine Liste mit allen Konten, die zu diesem Fach gehören.
     * @since 1.0
     */
    List<Account> findBySubjectId(Integer subjectId);

    List<Account> findByIdIn(List<Integer> accountIds);

    void deleteBySubjectIdAndStudentIdIn(Integer subjectId, List<Integer> studentIds);
}
