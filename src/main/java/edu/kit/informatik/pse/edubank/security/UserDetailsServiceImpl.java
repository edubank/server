package edu.kit.informatik.pse.edubank.security;

import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Diese Klasse implementiert die Authentifizierungslogik von Spring Security für die Benutzerdaten.
 *
 * @author EduBank Team
 * @version 1.0
 * @see UserDetailsService
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    /**
     * Referenz zu dem Benutzer-Repository; wird zur Suchen nach Benutzername verwendet
     */
    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;

        if (userRepository.count() == 0) {
            User defaultAdmin = User.builder()
                    .username("admin")
                    .password(new BCryptPasswordEncoder().encode("admin"))
                    .firstName("Admin")
                    .lastName("")
                    .email("admin@email.com")
                    .role(User.UserRole.ROLE_ADMIN)
                    .build();
            userRepository.saveAndFlush(defaultAdmin);
        }

    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username not found."));

        return new AuthenticatedUser(user);
    }
}
