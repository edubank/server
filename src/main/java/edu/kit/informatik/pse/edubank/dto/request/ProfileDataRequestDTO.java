package edu.kit.informatik.pse.edubank.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;

/**
 * Ein ProfileDataRequestDTO bündelt die E-Mail-Adresse (email) und das Passwort eines Benutzers (password) für eine
 * Profilanfrage.
 *
 * @author EduBank Team
 * @version 1.0
 */
@Data
@Builder
@AllArgsConstructor
public class ProfileDataRequestDTO {
    private String password;
    @Email(message = "Eine gültige E-Mail-Adresse ist erforderlich.")
    private String email;
}
