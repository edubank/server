package edu.kit.informatik.pse.edubank.security;

import edu.kit.informatik.pse.edubank.controller.AccountController;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von UserDetailsServiceImpl
 *
 * @see UserDetailsServiceImpl
 */
@ExtendWith(MockitoExtension.class)
class UserDetailsServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserDetailsServiceImpl userDetailsService;

    /**
     * Testet das Filtern von Benutzern nach ungültigem Benutzername
     */
    @Test
    void loadUserByUsernameInvalid() {
        String username = "username";
        when(userRepository.findByUsername(username)).thenReturn(Optional.empty());
        assertThrows(UsernameNotFoundException.class, () -> userDetailsService.loadUserByUsername(username));
    }

    /**
     * Testet das Filtern von Benutzern nach gültigem Benutzername
     */
    @Test
    void loadUserByUsernameValid() {
        String username = "username";

        User user = User.builder()
                .username(username)
                .build();

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        assertThat(userDetailsService.loadUserByUsername(username)).isEqualTo(new AuthenticatedUser(user));
    }
}