package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.request.SchoolClassRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.SchoolClassResponseDTO;
import edu.kit.informatik.pse.edubank.entity.SchoolClass;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.service.ClassService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.CLASS;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von ClassController
 *
 * @see ClassController
 */
@ExtendWith(MockitoExtension.class)
class ClassControllerTest {

    private static final Integer CLASS_ID = 1;
    private static final Integer CLASS2_ID = 2;
    private static final Integer CLASS3_ID = 3;

    private static final SchoolClass CLASS1 = SchoolClass.builder()
            .id(CLASS_ID)
            .name("class 1")
            .subject(Subject.builder().id(1).name("subject").build())
            .build();

    private static final SchoolClass CLASS2 = SchoolClass.builder()
            .id(CLASS2_ID)
            .name("class 2")
            .build();

    private static final SchoolClass CLASS3 = SchoolClass.builder()
            .id(CLASS3_ID)
            .name("class 3")
            .build();

    private static final List<SchoolClass> SCHOOL_CLASS_LIST = List.of(CLASS1, CLASS2, CLASS3);

    private static final List<SchoolClassResponseDTO> SCHOOL_CLASS_RESPONSE_DTO_LIST = List.of(
            new SchoolClassResponseDTO(CLASS1),
            new SchoolClassResponseDTO(CLASS2),
            new SchoolClassResponseDTO(CLASS3)
    );

    @Mock
    private ClassService classService;

    @InjectMocks
    private ClassController classController;


    /**
     * Testet das Aufrufen von allen Klassen
     */
    @Test
    void findAll() {
        when(classService.findAll()).thenReturn(SCHOOL_CLASS_LIST);
        assertThat(classController.findAll().getBody()).isEqualTo(SCHOOL_CLASS_RESPONSE_DTO_LIST);
    }

    /**
     * Testet das Filtern von Klassen nach ID
     */
    @Test
    void findByIdExisting() {
        when(classService.findById(CLASS_ID)).thenReturn(CLASS1);
        assertThat(classController.findById(CLASS_ID).getBody()).isEqualTo(new SchoolClassResponseDTO(CLASS1));
    }

    /**
     * Testet, ob ein Fehler auftritt, wenn man nach Klassen mit falscher ID sucht
     */
    @Test
    void findByIdNonExisting() {
        when(classService.findById(CLASS_ID)).thenReturn(null);
        assertThat(classController.findById(CLASS_ID).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    /**
     * Testet das Erzeugung von Klassen
     */
    @Test
    void createClass() {
        SchoolClass schoolClass = SchoolClass.builder().name("class 1").build();
        SchoolClassRequestDTO schoolClassRequestDTO = SchoolClassRequestDTO.builder().name("class 1").build();
        when(classService.createClass(schoolClass)).thenReturn(schoolClass);
        assertThat(classController.createClass(schoolClassRequestDTO).getBody()).isEqualTo(new SchoolClassResponseDTO(schoolClass));
    }

    /**
     * Testet die Aktualisierung von Klassen
     */
    @Test
    void updateClass() {
        SchoolClass schoolClass = SchoolClass.builder().name("class 1").build();
        SchoolClassRequestDTO schoolClassRequestDTO = SchoolClassRequestDTO.builder().name("class 1").build();
        when(classService.updateClass(CLASS_ID, schoolClass)).thenReturn(schoolClass);
        assertThat(classController.updateClass(CLASS_ID, schoolClassRequestDTO).getBody()).isEqualTo(new SchoolClassResponseDTO(schoolClass));

    }

    /**
     * Testet das Löschen von Klassen
     */
    @Test
    void deleteClassExistingID() {
        when(classService.deleteClassById(CLASS_ID)).thenReturn(true);
        assertThat(classController.deleteClass(CLASS_ID).getBody()).isTrue();
    }

    /**
     * Testet das Löschen von Klassen mit ungültiger ID
     */
    @Test
    void deleteClassNonExistingID() {
        when(classService.deleteClassById(CLASS_ID)).thenReturn(false);
        assertThat(classController.deleteClass(CLASS_ID).getBody()).isFalse();
    }
}