package edu.kit.informatik.pse.edubank.service.impl;

import edu.kit.informatik.pse.edubank.controller.AccountController;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.repository.AccountRepository;
import edu.kit.informatik.pse.edubank.repository.SubjectRepository;
import edu.kit.informatik.pse.edubank.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von UserServiceImpl
 *
 * @see UserServiceImpl
 */
@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private static final Integer STUDENT_ID = 1;
    private static final Integer STUDENT2_ID = 2;
    private static final Integer ADMIN_ID = 3;
    private static final Integer TEACHER_ID = 4;

    private static final String STUDENT_MAIL = "mail@mail.de";
    private static final String STUDENT_PASS = "password";

    private static final User TEACHER = User.builder()
            .id(TEACHER_ID)
            .role(User.UserRole.ROLE_TEACHER)
            .build();

    private static final Subject SUBJECT = Subject.builder()
            .name("subject1")
            .teacher(TEACHER)
            .build();

    private static final User ADMIN = User.builder()
            .id(ADMIN_ID)
            .role(User.UserRole.ROLE_ADMIN)
            .build();

    private static final User STUDENT = User.builder()
            .id(STUDENT_ID)
            .role(User.UserRole.ROLE_STUDENT)
            .email(STUDENT_MAIL)
            .subjects(List.of(SUBJECT))
            .password(new BCryptPasswordEncoder().encode(STUDENT_PASS))
            .build();

    private static final User STUDENT2 = User.builder()
            .role(User.UserRole.ROLE_STUDENT)
            .subjects(List.of(SUBJECT))
            .build();

    private static final List<User> STUDENT_LIST = List.of(STUDENT, STUDENT2);
    private static final List<User> USER_LIST = List.of(STUDENT, STUDENT2, TEACHER, ADMIN);

    @Mock
    private JavaMailSender emailSender;
    @Mock
    private UserRepository userRepository;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private SubjectRepository subjectRepository;

    @InjectMocks
    private UserServiceImpl userService;

    /**
     * Testet das Aufrufen von allen Benutzern im System
     */
    @Test
    void findAll() {
        when(userRepository.findAll()).thenReturn(USER_LIST);
        assertThat(userService.findAll()).isEqualTo(USER_LIST);
    }

    /**
     * Testet das Filtern von Benutzern nach ID
     */
    @Test
    void findById() {
        when(userRepository.findById(STUDENT_ID)).thenReturn(Optional.of(STUDENT));
        assertThat(userService.findById(STUDENT_ID)).isEqualTo(STUDENT);
    }

    /**
     * Testet das Filtern von Schülern nach SubjectID
     */
    @Test
    void findBySubjectId() {
        when(userRepository.findBySubjects_Id(SUBJECT.getId())).thenReturn(STUDENT_LIST);
        assertThat(userService.findBySubjectId(SUBJECT.getId())).isEqualTo(STUDENT_LIST);
    }

    /**
     * Testet das Filtern von Benutzern nach Rolle
     */
    @Test
    void findByRole() {
        when(userRepository.findByRole(User.UserRole.ROLE_STUDENT)).thenReturn(STUDENT_LIST);
        assertThat(userService.findByRole(User.UserRole.ROLE_STUDENT)).isEqualTo(STUDENT_LIST);
    }

    /**
     * Testet das Erstellen von Benutzern mit existierender Email
     */
    @Test
    void createUserExistingEmail() {
        User user = User.builder().email(STUDENT_MAIL).build();
        when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(STUDENT));
        assertThrows(ValidationException.class, () -> userService.createUser(user));
    }

    /**
     * Testet das Erstellen von Benutzern
     */
    @Test
    void createUser() {
        User user = User.builder().email(STUDENT_MAIL).role(User.UserRole.ROLE_STUDENT).build();
        when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.empty());
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        when(userRepository.saveAndFlush(user)).thenReturn(user);
        assertThat(userService.createUser(user)).isEqualTo(user);
    }

    /**
     * Testet die Aktualisierung von Benutzern mit ungültiger ID
     */
    @Test
    void updateUserInvalidID() {
        User user = User.builder().build();
        when(userRepository.findById(STUDENT_ID)).thenReturn(Optional.empty());
        assertThrows(ValidationException.class, () -> userService.updateUser(STUDENT_ID, user));
    }

    /**
     * Testet die Aktualisierung von Benutzern mit ungültiger Email
     */
    @Test
    void updateUserExistingEmail() {
        User user = User.builder().id(STUDENT2_ID).email(STUDENT_MAIL).role(User.UserRole.ROLE_STUDENT).build();
        when(userRepository.findById(STUDENT2_ID)).thenReturn(Optional.of(STUDENT2));
        when(userRepository.findByEmail(STUDENT_MAIL)).thenReturn(Optional.of(STUDENT));
        assertThrows(ValidationException.class, () -> userService.updateUser(STUDENT2_ID, user));
    }

    /**
     * Testet die Aktualisierung der Rolle von Schülern
     */
    @Test
    void updateUserChangeRoleOfStudent() {
        User user = User.builder().id(STUDENT_ID).role(User.UserRole.ROLE_TEACHER).build();
        when(userRepository.findById(STUDENT_ID)).thenReturn(Optional.of(STUDENT));
        assertThrows(ValidationException.class, () -> userService.updateUser(STUDENT_ID, user));
    }

    /**
     * Testet die Aktualisierung der Rolle von Admins und Lehrers auf die Schüler-Rolle
     */
    @Test
    void updateUserChangeRoleToStudent() {
        User user = User.builder().id(ADMIN_ID).role(User.UserRole.ROLE_STUDENT).build();
        when(userRepository.findById(ADMIN_ID)).thenReturn(Optional.of(ADMIN));
        assertThrows(ValidationException.class, () -> userService.updateUser(ADMIN_ID, user));
    }

    /**
     * Testet die Aktualisierung des Passwort mit dem gleichen Passwort
     */
    @Test
    void updateUserSamePasswordAsOld() {
        User user = User.builder().id(STUDENT_ID).password(STUDENT_PASS).role(User.UserRole.ROLE_STUDENT).build();
        when(userRepository.findById(STUDENT_ID)).thenReturn(Optional.of(STUDENT));
        assertThrows(ValidationException.class, () -> userService.updateUser(STUDENT_ID, user));
    }

    /**
     * Testet die Aktualisierung des Passworts
     */
    @Test
    void updateUserNewPassword() {
        User user = User.builder().id(STUDENT_ID).password(STUDENT_PASS + "123").role(User.UserRole.ROLE_STUDENT).build();
        when(userRepository.findById(STUDENT_ID)).thenReturn(Optional.of(STUDENT));
        when(userRepository.saveAndFlush(user)).thenReturn(user);
        assertThat(userService.updateUser(STUDENT_ID, user)).isEqualTo(user);
    }

    /**
     * Testet die Aktualisierung mit Generierung von neuem Passwort
     */
    @Test
    void updateUserNewGenPassword() {
        User user = User.builder().id(STUDENT_ID).role(User.UserRole.ROLE_STUDENT).build();
        when(userRepository.findById(STUDENT_ID)).thenReturn(Optional.of(STUDENT));
        when(userRepository.saveAndFlush(user)).thenReturn(user);
        assertThat(userService.updateUser(STUDENT_ID, user)).isEqualTo(user);
    }

    /**
     * Testet das Löschen von Benutzern mit ungültiger ID
     */
    @Test
    void deleteByInvalidID() {
        when(userRepository.findById(STUDENT_ID)).thenReturn(Optional.empty());
        assertThat(userService.deleteUserById(STUDENT_ID)).isFalse();
    }

    /**
     * Testet das Löschen vom letzen Admin
     */
    @Test
    void deleteAdminWhenLast() {
        when(userRepository.findById(ADMIN_ID)).thenReturn(Optional.of(ADMIN));
        when(userRepository.findByRole(User.UserRole.ROLE_ADMIN)).thenReturn(List.of(ADMIN));
        assertThrows(ValidationException.class, () -> userService.deleteUserById(ADMIN_ID));
    }

    /**
     * Testet das Löschen von Lehrer von Fächern
     */
    @Test
    void deleteTeacherOfSubject() {
        when(userRepository.findById(TEACHER_ID)).thenReturn(Optional.of(TEACHER));
        when(subjectRepository.findByTeacherId(TEACHER_ID)).thenReturn(List.of(SUBJECT));
        assertThrows(ValidationException.class, () -> userService.deleteUserById(TEACHER_ID));
    }

    /**
     * Testet das Löschen von Benutzern mit gültiger ID
     */
    @Test
    void deleteValidID() {
        when(userRepository.findById(STUDENT_ID)).thenReturn(Optional.of(STUDENT));
        assertThat(userService.deleteUserById(STUDENT_ID)).isTrue();
    }

    /**
     * Testet das Zurücksetzen vom Passwort mit ungültiger Email
     */
    @Test
    void resetPasswordNonExistingEmail() {
        when(userRepository.findByEmail(STUDENT_MAIL)).thenReturn(Optional.empty());
        assertThrows(ValidationException.class, () -> userService.resetPassword(STUDENT_MAIL));
    }

    /**
     * Testet das Zurücksetzen vom Passwort mit gültiger Email
     */
    @Test
    void resetPasswordExistingEmail() {
        when(userRepository.findByEmail(STUDENT_MAIL)).thenReturn(Optional.of(STUDENT));
        assertThatNoException().isThrownBy(() -> userService.resetPassword(STUDENT_MAIL));
    }
}