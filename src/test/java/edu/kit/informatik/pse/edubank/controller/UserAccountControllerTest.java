package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.request.ProfileDataRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.UserResponseDTO;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von UserAccountController
 *
 * @see UserAccountController
 */
@ExtendWith(MockitoExtension.class)
class UserAccountControllerTest {

    private static final AuthenticatedUser AUTH_USER = new AuthenticatedUser(
            User.builder().id(1).role(User.UserRole.ROLE_STUDENT).email("mail@mail.de").build()
    );

    @Mock
    private UserService userService;

    @InjectMocks
    private UserAccountController userAccountController;

    /**
     * Testet das Aufrufen vom persönlichen Konto
     */
    @Test
    void getMyUserAccount() {
        assertThat(userAccountController.getMyUserAccount(AUTH_USER).getBody()).isEqualTo(new UserResponseDTO(AUTH_USER.getUser()));
    }

    /**
     * Testet die Aktualisierung vom persönlichen Konto
     */
    @Test
    void updateMyUserAccount() {
        ProfileDataRequestDTO requestDTO = ProfileDataRequestDTO.builder()
                .password("pass123")
                .email("mail@mail.de")
                .build();

        when(userService.updateUser(AUTH_USER.getUser().getId(), AUTH_USER.getUser())).thenReturn(AUTH_USER.getUser());
        assertThat(userAccountController.updateMyUserAccount(requestDTO, AUTH_USER).getBody()).isEqualTo(new UserResponseDTO(AUTH_USER.getUser()));
    }
}