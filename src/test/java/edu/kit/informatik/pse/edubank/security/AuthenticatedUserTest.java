package edu.kit.informatik.pse.edubank.security;

import edu.kit.informatik.pse.edubank.controller.AccountController;
import edu.kit.informatik.pse.edubank.entity.User;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

import static edu.kit.informatik.pse.edubank.entity.User.UserRole.ROLE_TEACHER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Klasse für Unit-Testing von AuthenticatedUser
 *
 * @see AuthenticatedUser
 */
class AuthenticatedUserTest {

    private static final User TEACHER = User.builder()
            .username("username")
            .password("password")
            .role(ROLE_TEACHER)
            .build();

    private AuthenticatedUser authenticatedUser = new AuthenticatedUser(TEACHER);

    /**
     * Testet das Aufrufen von Permission von einem Benutzer
     */
    @Test
    void getAuthorities() {
        assertThat(authenticatedUser.getAuthorities()).isEqualTo(List.of(new SimpleGrantedAuthority(ROLE_TEACHER.toString())));
    }

    /**
     * Testet das Aufrufen von Passwort von einem Benutzer
     */
    @Test
    void getPassword() {
        assertThat(authenticatedUser.getPassword()).isEqualTo(TEACHER.getPassword());
    }

    /**
     * Testet das Aufrufen von Benutzername von einem Benutzer
     */
    @Test
    void getUsername() {
        assertThat(authenticatedUser.getUsername()).isEqualTo(TEACHER.getUsername());
    }

    /**
     * Testet, ob alle Benutzer gültiges Konto haben
     */
    @Test
    void isAccountNonExpired() {
        assertThat(authenticatedUser.isAccountNonExpired()).isTrue();
    }

    /**
     * Testet, ob alle Benutzer gültiges Konto haben
     */
    @Test
    void isAccountNonLocked() {
        assertThat(authenticatedUser.isAccountNonLocked()).isTrue();
    }

    /**
     * Testet, ob alle Benutzer gültiges Konto haben
     */
    @Test
    void isCredentialsNonExpired() {
        assertThat(authenticatedUser.isCredentialsNonExpired()).isTrue();
    }

    /**
     * Testet, ob alle Benutzer gültiges Konto haben
     */
    @Test
    void isEnabled() {
        assertThat(authenticatedUser.isEnabled()).isTrue();
    }

    /**
     * Testet das Aufrufen vom User-Objekt aus dem AuthenticatedUser
     */
    @Test
    void getUser() {
        assertThat(authenticatedUser.getUser()).isEqualTo(TEACHER);
    }
}