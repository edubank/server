package edu.kit.informatik.pse.edubank.service.impl;

import edu.kit.informatik.pse.edubank.controller.AccountController;
import edu.kit.informatik.pse.edubank.entity.SchoolClass;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.repository.ClassRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von ClassServiceImpl
 *
 * @see ClassServiceImpl
 */
@ExtendWith(MockitoExtension.class)
class ClassServiceImplTest {

    private static final List<SchoolClass> CLASS_LIST = List.of(
            SchoolClass.builder().id(1).name("10a").build(),
            SchoolClass.builder().id(2).name("11b").build(),
            SchoolClass.builder().id(3).name("12c").build()
    );

    private static final SchoolClass SCHOOL_CLASS = SchoolClass.builder().id(1).name("10a").build();

    private static final int ID = 10;
    private static final String CLASS_NAME = "10c";

    @Mock
    private ClassRepository classRepository;

    @InjectMocks
    private ClassServiceImpl classService;

    /**
     * Testet das Aufrufen von allen Klassen im System
     */
    @Test
    void findAllClasses() {
        when(classRepository.findAll()).thenReturn(CLASS_LIST);
        assertThat(classService.findAll()).isEqualTo(CLASS_LIST);
    }

    /**
     * Testet das Filtern von Klassen nach ID
     */
    @Test
    void findByIdWithExistingID() {
        when(classRepository.findById(anyInt())).thenReturn(Optional.of(SCHOOL_CLASS));
        assertThat(classService.findById(anyInt())).isEqualTo(SCHOOL_CLASS);
    }

    /**
     * Testet das Filtern von Klassen nach ungültiger ID
     */
    @Test
    void findByIdWithNonExistingID() {
        when(classRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThat(classService.findById(anyInt())).isNull();
    }

    /**
     * Testet das Erstellen von Klassen
     */
    @Test
    void createClassWithCorrectParameters() {
        when(classRepository.save(SCHOOL_CLASS)).thenReturn(SCHOOL_CLASS);
        assertThat(classService.createClass(SCHOOL_CLASS).getName()).isEqualTo(SCHOOL_CLASS.getName());
    }

    /**
     * Testet die Aktualisierung von Klassen
     */
    @Test
    void updateClassWithExistingId() {
        when(classRepository.findById(ID)).thenReturn(Optional.of(SchoolClass.builder().id(ID).name("class name").build()));
        SchoolClass newClass = SchoolClass.builder().id(ID).name(CLASS_NAME).build();
        when(classRepository.save(newClass)).thenReturn(newClass);

        assertThat(classService.updateClass(ID, newClass)).isEqualTo(newClass);
    }

    /**
     * Testet die Aktualisierung von Klassen mit ungültiger ID
     */
    @Test
    void updateClassWithNonExistingId() {
        when(classRepository.findById(ID)).thenReturn(Optional.empty());
        assertThat(classService.updateClass(ID, SCHOOL_CLASS)).isNull();
    }

    /**
     * Testet das Löschen von Klassen mit gültiger ID
     */
    @Test
    void deleteClassByExistingId() {
        when(classRepository.findById(SCHOOL_CLASS.getId())).thenReturn(Optional.of(SCHOOL_CLASS));
        assertThat(classService.deleteClassById(SCHOOL_CLASS.getId())).isTrue();
    }

    /**
     * Testet das Löschen von Klassen mit existierenden Fächern
     */
    @Test
    void deleteClassWithSubjects() {
        SchoolClass schoolClass = SchoolClass.builder().id(ID).name("10a").subject(Subject.builder().name("Mathe").build()).build();
        when(classRepository.findById(anyInt())).thenReturn(Optional.of(schoolClass));
        assertThrows(ValidationException.class, () -> classService.deleteClassById(ID));
    }

    /**
     * Testet das Löschen von Klassen mit ungültiger ID
     */
    @Test
    void deleteClassByNonExistingId() {
        when(classRepository.findById(ID)).thenReturn(Optional.empty());
        assertThat(classService.deleteClassById(ID)).isFalse();
    }
}