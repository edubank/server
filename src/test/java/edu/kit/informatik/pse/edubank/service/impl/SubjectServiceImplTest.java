package edu.kit.informatik.pse.edubank.service.impl;

import edu.kit.informatik.pse.edubank.controller.AccountController;
import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.SchoolClass;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.repository.AccountRepository;
import edu.kit.informatik.pse.edubank.repository.ClassRepository;
import edu.kit.informatik.pse.edubank.repository.SubjectRepository;
import edu.kit.informatik.pse.edubank.repository.UserRepository;
import edu.kit.informatik.pse.edubank.service.SubjectService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.InvalidClassException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von SubjectServiceImpl
 *
 * @see SubjectServiceImpl
 */
@ExtendWith(MockitoExtension.class)
class SubjectServiceImplTest {

    private static final List<Subject> SUBJECT_LIST = List.of(
            Subject.builder().id(1).schoolClass(SchoolClass.builder()
                    .id(10).build())
                .name("Mathe")
                .teacher(User.builder()
                    .id(1)
                    .firstName("Moritz")
                    .lastName("Wagner")
                    .role(User.UserRole.ROLE_TEACHER)
                    .build())
                .build(),
            Subject.builder().id(2).schoolClass(SchoolClass.builder()
                    .id(11)
                    .build())
                .name("Bio")
                .teacher(User.builder()
                    .id(1)
                    .firstName("Moritz")
                    .lastName("Wagner")
                    .role(User.UserRole.ROLE_TEACHER)
                    .build())
                .build(),
            Subject.builder().id(3).schoolClass(SchoolClass.builder()
                    .id(12)
                    .build())
                .name("Deutsch")
                .teacher(User.builder()
                    .id(1)
                    .firstName("Moritz")
                    .lastName("Wagner")
                    .role(User.UserRole.ROLE_TEACHER)
                    .build())
                .build()
    );

    @Mock
    private SubjectRepository subjectRepository;

    @Mock
    private ClassRepository classRepository;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private SubjectServiceImpl subjectService;

    /**
     * Testet das Aufrufen von allen Fächern im System
     */
    @Test
    void findAll() {
        when(subjectRepository.findAll()).thenReturn(SUBJECT_LIST);
        assertThat(subjectService.findAll()).isEqualTo(SUBJECT_LIST);
    }

    /**
     * Testet das Filtern von Fächern nach ID
     */
    @Test
    void findByExistingId() {
        when(subjectRepository.findById(SUBJECT_LIST.get(0).getId())).thenReturn(Optional.of(SUBJECT_LIST.get(0)));
        assertThat(subjectService.findById(SUBJECT_LIST.get(0).getId())).isEqualTo(SUBJECT_LIST.get(0));
    }

    /**
     * Testet das Filtern von Fächern nach ungültiger ID
     */
    @Test
    void findByNonExistingId() {
        when(subjectRepository.findById(SUBJECT_LIST.get(0).getId())).thenReturn(Optional.empty());
        assertThat(subjectService.findById(SUBJECT_LIST.get(0).getId())).isNull();
    }

    /**
     * Testet das Filtern von Fächern nach ClassID
     */
    @Test
    void findByClassId() {
        when(subjectRepository
                .findBySchoolClassId(SUBJECT_LIST
                .get(0)
                .getSchoolClass()
                .getId()))
                .thenReturn(List.of(SUBJECT_LIST.get(0)));
        assertThat(subjectService.findByClassId(SUBJECT_LIST.get(0).getSchoolClass().getId())).isEqualTo(List.of(SUBJECT_LIST.get(0)));
    }

    /**
     * Testet das Filtern von Fächern nach TeacherID
     */
    @Test
    void findByTeacherId() {
        when(subjectRepository.findByTeacherId(SUBJECT_LIST.get(0).getTeacher().getId())).thenReturn(SUBJECT_LIST);
        assertThat(subjectService.findByTeacherId(SUBJECT_LIST.get(0).getTeacher().getId())).isEqualTo(SUBJECT_LIST);
    }

    /**
     * Testet das Erstellen von Fächern mit ungültiger SchoolClassID
     */
    @Test
    void createSubjectInvalidSchoolClassID() {
        Subject subject = Subject.builder()
                .schoolClass(SchoolClass.builder()
                        .id(10)
                        .build())
                .teacher(User.builder()
                        .id(20)
                        .role(User.UserRole.ROLE_TEACHER)
                        .build())
                .build();

        when(classRepository.findById(subject.getSchoolClass().getId())).thenReturn(Optional.empty());
        assertThrows(ValidationException.class, () -> subjectService.createSubject(subject));
    }

    /**
     * Testet das Erstellen von Fächern mit ungültiger Rolle vom Lehrer
     */
    @Test
    void createSubjectWithInvalidTeacherRole() {
        User teacher = User.builder()
                .id(20)
                .role(User.UserRole.ROLE_STUDENT)
                .build();

        Subject subject = Subject.builder()
                .teacher(teacher)
                .build();

        when(userRepository.findById(subject.getTeacher().getId())).thenReturn(Optional.of(teacher));
        assertThrows(ValidationException.class, () -> subjectService.createSubject(subject));
    }

    /**
     * Testet das Erstellen von Fächern mit ungültiger TeacherID
     */
    @Test
    void createSubjectWithInvalidTeacherID() {
        Subject subject = Subject.builder()
                .teacher(User.builder()
                        .id(20)
                        .role(User.UserRole.ROLE_TEACHER)
                        .build())
                .build();

        when(userRepository.findById(subject.getTeacher().getId())).thenReturn(Optional.empty());
        assertThrows(ValidationException.class, () -> subjectService.createSubject(subject));
    }

    /**
     * Testet das Erstellen von Fächern mit gültiger SchoolClassID und gültiger TeacherID
     */
    @Test
    void createSubjectValidClassAndValidTeacher() {
        SchoolClass schoolClass = SchoolClass.builder()
                .id(10)
                .name("10a")
                .build();

        User teacher = User.builder()
                .id(20)
                .role(User.UserRole.ROLE_TEACHER)
                .build();

        Subject subject = Subject.builder()
                .schoolClass(schoolClass)
                .teacher(teacher)
                .build();

        when(classRepository.findById(subject.getSchoolClass().getId())).thenReturn(Optional.of(schoolClass));
        when(userRepository.findById(subject.getTeacher().getId())).thenReturn(Optional.of(teacher));
        when(subjectRepository.save(subject)).thenReturn(subject);
        assertThat(subjectService.createSubject(subject)).isEqualTo(subject);
    }

    /**
     * Testet die Aktualisierung von Fächern mit gültiger ID
     */
    @Test
    void updateSubjectExistingID() {
        SchoolClass schoolClass = SchoolClass.builder()
                .id(10)
                .name("10a")
                .build();

        User teacher = User.builder()
                .id(20)
                .role(User.UserRole.ROLE_TEACHER)
                .build();

        Subject subject = Subject.builder()
                .teacher(teacher)
                .build();

        int id = 10;

        when(classRepository.findById(schoolClass.getId())).thenReturn(Optional.of(schoolClass));
        when(userRepository.findById(teacher.getId())).thenReturn(Optional.of(teacher));
        when(subjectRepository.existsById(10)).thenReturn(true);


        Subject subjectWithClass = Subject.builder()
                .teacher(teacher)
                .schoolClass(schoolClass)
                .build();

        Subject subjectWithClassAndId = Subject.builder()
                .id(id)
                .teacher(teacher)
                .schoolClass(schoolClass)
                .build();

        when(subjectRepository.save(subjectWithClassAndId)).thenReturn(subjectWithClassAndId);
        assertThat(subjectService.updateSubject(id, subjectWithClass)).isEqualTo(subjectWithClassAndId);
    }

    /**
     * Testet die Aktualisierung von Fächern mit ungültiger ID
     */
    @Test
    void updateSubjectNonExistingID() {
        int id = 10;
        when(subjectRepository.existsById(id)).thenReturn(false);
        assertThrows(ValidationException.class, () -> subjectService.updateSubject(id, any(Subject.class)));
    }

    /**
     * Testet das Hinzufügen von Schülern zu einem Fach mit ungültiger ID
     */
    @Test
    void addStudentsToInvalidSubject() {
        Integer id = 10;
        when(subjectRepository.findById(id)).thenReturn(Optional.empty());
        assertThrows(ValidationException.class, () -> subjectService.addStudentsToSubject(id, new ArrayList<>()));
    }

    /**
     * Testet das Hinzufügen von Schülern mit ungültigen IDs zu einem Fach
     */
    @Test
    void addStudentsWithInvalidStudentIdInList() {
        List<User> students = List.of(
                User.builder().id(10).role(User.UserRole.ROLE_STUDENT).build(),
                User.builder().id(11).role(User.UserRole.ROLE_STUDENT).build(),
                User.builder().id(12).role(User.UserRole.ROLE_STUDENT).build()
        );

        List<Integer> studentIds = List.of(10, 11, 12, 13);

        when(subjectRepository.findById(any())).thenReturn(Optional.of(Subject.builder().build()));
        when(userRepository.findByIdIn(studentIds)).thenReturn(students);

        assertThrows(ValidationException.class, () -> subjectService.addStudentsToSubject(10, studentIds));
    }

    /**
     * Testet das Hinzufügen von Schülern mit ungültigen Rollen zu einem Fach
     */
    @Test
    void addStudentsWithInvalidStudentRoleInList() {
        List<User> students = List.of(
                User.builder().id(10).role(User.UserRole.ROLE_STUDENT).build(),
                User.builder().id(11).role(User.UserRole.ROLE_STUDENT).build(),
                User.builder().id(12).role(User.UserRole.ROLE_ADMIN).build()
        );

        List<Integer> studentIds = List.of(10, 11, 12);

        when(subjectRepository.findById(any())).thenReturn(Optional.of(Subject.builder().build()));
        when(userRepository.findByIdIn(studentIds)).thenReturn(students);

        assertThrows(ValidationException.class, () -> subjectService.addStudentsToSubject(any(), studentIds));
    }

    /**
     * Testet das Hinzufügen von Schülern zu einem Fach, zu dem die schon gehören
     */
    @Test
    void addStudentsWithStudentAlreadyInSubject() {

        int studentId = 10;
        int subjectId = 20;

        User student = User.builder()
                .id(studentId)
                .role(User.UserRole.ROLE_STUDENT)
                .build();

        Subject subject = Subject.builder()
                .id(subjectId)
                .account(Account.builder()
                        .student(student)
                        .build())
                .build();

        when(subjectRepository.findById(subjectId)).thenReturn(Optional.of(subject));
        when(userRepository.findByIdIn(List.of(studentId))).thenReturn(List.of(student));

        assertThrows(ValidationException.class, () -> subjectService.addStudentsToSubject(subjectId, List.of(studentId)));
    }

    /**
     * Testet das Hinzufügen von Schülern mit gültigen IDs zu einem Fach
     */
    @Test
    void addNewStudentToSubject() {
        int studentId = 10;
        int subjectId = 20;

        User student = User.builder()
                .id(studentId)
                .role(User.UserRole.ROLE_STUDENT)
                .build();

        Subject subject = Subject.builder()
                .id(subjectId)
                .accounts(new ArrayList<>())
                .build();

        Account account = Account.builder()
                .student(student)
                .subject(subject)
                .build();

        when(subjectRepository.findById(subjectId)).thenReturn(Optional.of(subject));
        when(userRepository.findByIdIn(List.of(studentId))).thenReturn(List.of(student));
        when(accountRepository.findBySubjectId(subjectId)).thenReturn(List.of(account));

        assertThat(subjectService.addStudentsToSubject(subjectId, List.of(studentId))).isEqualTo(List.of(account));
    }

    /**
     * Testet das Entfernen von Schülern mit gültigen IDs aus einem Fach
     */
    @Test
    void removeStudentsFromSubject() {
        int studentId = 10;
        int subjectId = 20;

        User student = User.builder()
                .id(studentId)
                .role(User.UserRole.ROLE_STUDENT)
                .build();

        Account account = Account.builder()
                .student(student)
                .build();

        Subject subject = Subject.builder()
                .id(subjectId)
                .account(account)
                .build();

        when(subjectRepository.findById(subjectId)).thenReturn(Optional.of(subject));
        when(userRepository.findByIdIn(List.of(studentId))).thenReturn(List.of(student));
        when(accountRepository.findBySubjectId(subjectId)).thenReturn(List.of());

        assertThat(subjectService.removeStudentsFromSubject(subjectId, List.of(studentId))).isEqualTo(new ArrayList<Account>());

    }

    /**
     * Testet das Löschen von einem Fach nach ID
     */
    @Test
    void deleteSubjectByExistingId() {
        int subjectId = 10;
        when(subjectRepository.findById(subjectId)).thenReturn(Optional.of(Subject.builder().build()));
        assertThat(subjectService.deleteSubjectById(subjectId)).isTrue();
    }

    /**
     * Testet das Löschen von einem Fach nach ungültiger ID
     */
    @Test
    void deleteSubjectByNonExistingId() {
        int subjectId = 10;
        when(subjectRepository.findById(subjectId)).thenReturn(Optional.empty());
        assertThat(subjectService.deleteSubjectById(subjectId)).isFalse();
    }

    /**
     * Testet die Überprüfung von Rechten von einem Schüler für ein Fach
     */
    @Test
    void hasPermissionStudent() {
        User student = User.builder().role(User.UserRole.ROLE_STUDENT).build();
        assertThat(subjectService.hasPermission(student, Subject.builder().build())).isFalse();
    }

    /**
     * Testet die Überprüfung von Rechten von einem Admin für ein Fach
     */
    @Test
    void hasPermissionAdmin() {
        User admin = User.builder().role(User.UserRole.ROLE_ADMIN).build();
        assertThat(subjectService.hasPermission(admin, Subject.builder().build())).isTrue();
    }

    /**
     * Testet die Überprüfung von Rechten von einem Lehrer für ein Fach
     */
    @Test
    void hasPermissionTeacherOfSubject() {
        User teacher = User.builder()
                .id(10)
                .role(User.UserRole.ROLE_TEACHER)
                .build();

        Subject subject = Subject.builder()
                .teacher(teacher)
                .build();

        assertThat(subjectService.hasPermission(teacher, subject)).isTrue();
    }

    /**
     * Testet die Überprüfung von Rechten von einem ungültigem Lehrer für ein Fach
     */
    @Test
    void hasPermissionNotTeacherOfSubject() {

        User teacher = User.builder()
                .id(1)
                .role(User.UserRole.ROLE_TEACHER)
                .build();

        User teacher2 = User.builder()
                .id(2)
                .role(User.UserRole.ROLE_TEACHER)
                .build();

        Subject subject = Subject.builder()
                .teacher(teacher)
                .build();

        assertThat(subjectService.hasPermission(teacher2, subject)).isFalse();
    }
}