package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.request.ForgotPasswordRequestDTO;
import edu.kit.informatik.pse.edubank.dto.request.LoginRequestDTO;
import edu.kit.informatik.pse.edubank.dto.request.ProfileDataRequestDTO;
import edu.kit.informatik.pse.edubank.dto.request.UserRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.UserResponseDTO;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.service.UserService;
import edu.kit.informatik.pse.edubank.service.impl.ValidationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.AuthenticationManager;

import javax.servlet.http.HttpSession;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von LoginController
 *
 * @see LoginController
 */
@ExtendWith(MockitoExtension.class)
class LoginControllerTest {

    private static final User ADMIN = User.builder()
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_ADMIN)
            .build();

    private static final AuthenticatedUser AUTHENTICATED_USER_ADMIN = new AuthenticatedUser(ADMIN);

    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private UserService userService;

    @InjectMocks
    private LoginController loginController;

    /**
     * Testet das Login ins System
     */
    @Test
    void login() {
        LoginRequestDTO loginRequestDTO = LoginRequestDTO.builder().username("admin").password("admin").build();
        HttpSession session = new MockHttpSession();
        assertThatNoException().isThrownBy(() -> loginController.login(loginRequestDTO, session));
    }

    /**
     * Testet das Logout aus dem System
     */
    @Test
    void logout() {
        assertThat(loginController.logout().getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    /**
     * Testet das initiale Setzen von neuem Passwort und Email ohne eingegebenes Passwort
     */
    @Test
    void setInitialDataNoPassword() {
        ProfileDataRequestDTO profileDataRequestDTO = ProfileDataRequestDTO.builder().password("").build();
        assertThrows(ValidationException.class, () -> loginController.setInitialData(profileDataRequestDTO, AUTHENTICATED_USER_ADMIN));
    }

    /**
     * Testet das initiale Setzen von neuem Passwort und Email ohne eingegebene Email
     */
    @Test
    void setInitialDataNoEmail() {
        ProfileDataRequestDTO profileDataRequestDTO = ProfileDataRequestDTO.builder().password("pass").email("").build();
        assertThrows(ValidationException.class, () -> loginController.setInitialData(profileDataRequestDTO, AUTHENTICATED_USER_ADMIN));
    }

    /**
     * Testet das initiale Setzen von neuem Passwort und Email
     */
    @Test
    void setInitialData() {
        ProfileDataRequestDTO profileDataRequestDTO = ProfileDataRequestDTO.builder().password("pass").email("mail@mail.de").build();

        User userResult = User.builder()
                .id(1)
                .firstName("name")
                .lastName("name")
                .email("mail@mail.de")
                .password("pass")
                .build();

        AuthenticatedUser authenticatedUser = new AuthenticatedUser(userResult);

        UserResponseDTO response = new UserResponseDTO(userResult);

        when(userService.updateUser(authenticatedUser.getUser().getId(), authenticatedUser.getUser())).thenReturn(userResult);
        assertThat(loginController.setInitialData(profileDataRequestDTO, authenticatedUser).getBody()).isEqualTo(response);
    }

    /**
     * Testet das Zurücksetzen von Passwörtern
     */
    @Test
    void forgotPassword() {
        ForgotPasswordRequestDTO forgotPasswordRequestDTO = ForgotPasswordRequestDTO.builder().email("mail@mail.de").build();
        assertThat(loginController.forgotPassword(forgotPasswordRequestDTO).getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}