package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.request.UserRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.UserResponseDTO;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.service.SubjectService;
import edu.kit.informatik.pse.edubank.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von UserController
 *
 * @see UserController
 */
@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    private static final Integer STUDENT_ID = 1;
    private static final Integer STUDENT2_ID = 2;
    private static final Integer ADMIN_ID = 3;
    private static final Integer TEACHER_ID = 4;
    private static final Integer SUBJECT_ID = 1;

    private static final String STUDENT_MAIL = "mail@mail.de";
    private static final String STUDENT_PASS = "password";

    private static final User TEACHER = User.builder()
            .id(TEACHER_ID)
            .role(User.UserRole.ROLE_TEACHER)
            .build();

    private static final Subject SUBJECT = Subject.builder()
            .id(SUBJECT_ID)
            .name("subject1")
            .teacher(TEACHER)
            .build();

    private static final User ADMIN = User.builder()
            .id(ADMIN_ID)
            .role(User.UserRole.ROLE_ADMIN)
            .build();

    private static final User STUDENT = User.builder()
            .id(STUDENT_ID)
            .role(User.UserRole.ROLE_STUDENT)
            .email(STUDENT_MAIL)
            .subjects(List.of(SUBJECT))
            .password(new BCryptPasswordEncoder().encode(STUDENT_PASS))
            .build();

    private static final User STUDENT2 = User.builder()
            .role(User.UserRole.ROLE_STUDENT)
            .subjects(List.of(SUBJECT))
            .build();

    private static final List<User> STUDENT_LIST = List.of(STUDENT, STUDENT2);
    private static final List<User> USER_LIST = List.of(STUDENT, STUDENT2, TEACHER, ADMIN);
    private static final List<UserResponseDTO> USER_LIST_DTO = List.of(
            new UserResponseDTO(STUDENT),
            new UserResponseDTO(STUDENT2),
            new UserResponseDTO(TEACHER),
            new UserResponseDTO(ADMIN)
    );

    private static final List<UserResponseDTO> STUDENT_LIST_DTO = List.of(
            new UserResponseDTO(STUDENT),
            new UserResponseDTO(STUDENT2)
    );

    private static final AuthenticatedUser AUTH_USER_STUDENT = new AuthenticatedUser(STUDENT);
    private static final AuthenticatedUser AUTH_USER_TEACHER = new AuthenticatedUser(TEACHER);
    private static final AuthenticatedUser AUTH_USER_ADMIN = new AuthenticatedUser(ADMIN);


    @Mock
    private UserService userService;
    @Mock
    private SubjectService subjectService;

    @InjectMocks
    private UserController userController;

    /**
     * Testet das Aufrufen von allen Bentuzern im System
     */
    @Test
    void findAll() {
        when(userService.findAll()).thenReturn(USER_LIST);
        assertThat(userController.findAll().getBody()).isEqualTo(USER_LIST_DTO);
    }

    /**
     * Testet das Filtern von Schülern nach ungültiger SubjectID
     */
    @Test
    void findBySubjectIdInvalidID() {
        when(subjectService.findById(anyInt())).thenReturn(null);
        assertThat(userController.findBySubjectId(0, AUTH_USER_ADMIN).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    /**
     * Testet das Filtern von Schülern ohne Autorisierung
     */
    @Test
    void findBySubjectIdUnauthorized() {
        when(subjectService.findById(anyInt())).thenReturn(SUBJECT);
        when(subjectService.hasPermission(AUTH_USER_STUDENT.getUser(), SUBJECT)).thenReturn(false);
        assertThat(userController.findBySubjectId(0, AUTH_USER_STUDENT).getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Testet das Filtern von Schülern nach SubjectID
     */
    @Test
    void findBySubjectId() {
        when(subjectService.findById(SUBJECT_ID)).thenReturn(SUBJECT);
        when(subjectService.hasPermission(AUTH_USER_STUDENT.getUser(), SUBJECT)).thenReturn(true);
        when(userService.findBySubjectId(SUBJECT_ID)).thenReturn(USER_LIST);
        assertThat(userController.findBySubjectId(SUBJECT_ID, AUTH_USER_STUDENT).getBody()).isEqualTo(USER_LIST_DTO);
    }

    /**
     * Testet das Filtern von Benutzern nach Rolle
     */
    @Test
    void findByRole() {
        when(userService.findByRole(User.UserRole.ROLE_STUDENT)).thenReturn(STUDENT_LIST);
        assertThat(userController.findByRole(User.UserRole.ROLE_STUDENT).getBody()).isEqualTo(STUDENT_LIST_DTO);
    }

    /**
     * Testet das Filtern von Benutzern nach ungültiger ID
     */
    @Test
    void findByIdInvalidId() {
        when(userService.findById(any())).thenReturn(null);
        assertThat(userController.findById(0).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    /**
     * Testet das Filtern von Benutzern nach ID
     */
    @Test
    void findById() {
        when(userService.findById(STUDENT_ID)).thenReturn(STUDENT);
        assertThat(userController.findById(STUDENT_ID).getBody()).isEqualTo(new UserResponseDTO(STUDENT));
    }

    /**
     * Testet das Erstellen von Benutzern
     */
    @Test
    void createUser() {
        UserRequestDTO userRequestDTO = UserRequestDTO.builder()
                .email("mail@mail.de")
                .role(User.UserRole.ROLE_STUDENT)
                .firstName("name")
                .lastName("name")
                .build();

        User entity = User.builder()
                .email("mail@mail.de")
                .role(User.UserRole.ROLE_STUDENT)
                .firstName("name")
                .lastName("name")
                .build();

        UserResponseDTO responseDTO = new UserResponseDTO(entity);

        when(userService.createUser(entity)).thenReturn(entity);
        assertThat(userController.createUser(userRequestDTO).getBody()).isEqualTo(responseDTO);
    }

    /**
     * Testet die Aktualisierung von Benutzerdaten
     */
    @Test
    void updateUser() {
        UserRequestDTO userRequestDTO = UserRequestDTO.builder()
                .email("mail@mail.de")
                .role(User.UserRole.ROLE_STUDENT)
                .firstName("name")
                .lastName("name")
                .build();

        User entity = User.builder()
                .email("mail@mail.de")
                .role(User.UserRole.ROLE_STUDENT)
                .firstName("name")
                .lastName("name")
                .build();

        UserResponseDTO responseDTO = new UserResponseDTO(entity);

        when(userService.updateUser(STUDENT_ID, entity)).thenReturn(entity);
        assertThat(userController.updateUser(STUDENT_ID, userRequestDTO).getBody()).isEqualTo(responseDTO);
    }

    /**
     * Testet das Löschen von Benutzern nach ungültiger ID
     */
    @Test
    void deleteUserInvalid() {
        when(userService.deleteUserById(STUDENT_ID)).thenReturn(false);
        assertThat(userController.deleteUser(STUDENT_ID).getBody()).isFalse();
    }

    /**
     * Testet das Löschen von Benutzern
     */
    @Test
    void deleteUser() {
        when(userService.deleteUserById(STUDENT_ID)).thenReturn(true);
        assertThat(userController.deleteUser(STUDENT_ID).getBody()).isTrue();
    }
}