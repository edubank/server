package edu.kit.informatik.pse.edubank.service.impl;

import edu.kit.informatik.pse.edubank.controller.AccountController;
import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.Transaction;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.repository.AccountRepository;
import edu.kit.informatik.pse.edubank.repository.TransactionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von TransactionServiceImpl
 *
 * @see TransactionServiceImpl
 */
@ExtendWith(MockitoExtension.class)
class TransactionServiceImplTest {

    private static final Integer STUDENT_ID = 9;
    private static final Integer STUDENT2_ID = 10;
    private static final Integer TEACHER_ID = 11;
    private static final Integer ADMIN_ID = 12;
    private static final Integer SUBJECT_ID = 20;

    private static final Integer ACCOUNT_ID = 1;
    private static final Integer ACCOUNT2_ID = 2;

    private static final User STUDENT = User.builder()
            .id(STUDENT_ID)
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User STUDENT2 = User.builder()
            .id(STUDENT2_ID)
            .firstName("Firstname2")
            .lastName("lastname2")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User TEACHER = User.builder()
            .id(TEACHER_ID)
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_TEACHER)
            .build();

    private static final User ADMIN = User.builder()
            .id(ADMIN_ID)
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_ADMIN)
            .build();

    private static final Subject SUBJECT = Subject.builder()
            .id(SUBJECT_ID)
            .teacher(TEACHER)
            .build();

    private static final Subject SUBJECT2 = Subject.builder()
            .id(SUBJECT_ID)
            .teacher(ADMIN)
            .build();

    private static final Account ACCOUNT = Account.builder()
            .id(ACCOUNT_ID)
            .subject(SUBJECT)
            .student(STUDENT)
            .build();

    private static final Account ACCOUNT2 = Account.builder()
            .id(ACCOUNT2_ID)
            .subject(SUBJECT2)
            .student(STUDENT2)
            .balance(20D)
            .build();

    private static final List<Transaction> TRANSACTION_LIST = List.of(
            Transaction.builder().id(1).account(ACCOUNT).initiator(ADMIN).amount(1D).title("transaction 1").build(),
            Transaction.builder().id(2).account(ACCOUNT).initiator(ADMIN).amount(5D).title("transaction 2").build(),
            Transaction.builder().id(3).account(ACCOUNT2).initiator(TEACHER).amount(10D).title("transaction 3").build()
    );

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private TransactionServiceImpl transactionService;

    /**
     * Testet das Aufrufen von allen Transaktionen im System
     */
    @Test
    void findAll() {
        when(transactionRepository.findAll()).thenReturn(TRANSACTION_LIST);
        assertThat(transactionService.findAll()).isEqualTo(TRANSACTION_LIST);
    }

    /**
     * Testet das Filtern von Transaktionen nach ID
     */
    @Test
    void findById() {
        Integer id = 10;
        Transaction transaction = Transaction.builder().id(id).build();
        when(transactionRepository.findById(id)).thenReturn(Optional.of(transaction));
        assertThat(transactionService.findById(id)).isEqualTo(transaction);
    }

    /**
     * Testet das Filtern von Transaktionen nach AccountID
     */
    @Test
    void findByAccountId() {
        List<Transaction> transactions = TRANSACTION_LIST.stream().filter(t -> t.getAccount().getId().equals(ACCOUNT_ID)).collect(Collectors.toList());
        when(transactionRepository.findByAccountId(ACCOUNT_ID)).thenReturn(transactions);
        assertThat(transactionService.findByAccountId(ACCOUNT_ID)).isEqualTo(transactions);
    }

    /**
     * Testet das Erzeugen von Transaktionen mit ungültiger AccountID
     */
    @Test
    void createTransactionInvalidAccountID() {
        Transaction transaction = Transaction.builder().account(ACCOUNT2).build();
        when(accountRepository.findById(ACCOUNT2_ID)).thenReturn(Optional.empty());
        assertThrows(ValidationException.class, () -> transactionService.createTransaction(transaction));
    }

    /**
     * Testet das Erzeugen von Transaktionen mit gültiger AccountID
     */
    @Test
    void createTransactionValidAccountID() {
        Transaction transaction = Transaction.builder().account(ACCOUNT2).amount(10D).build();
        when(accountRepository.findById(ACCOUNT2_ID)).thenReturn(Optional.of(ACCOUNT2));
        when(transactionRepository.save(transaction)).thenReturn(transaction);
        assertThat(transactionService.createTransaction(transaction)).isEqualTo(transaction);
    }

    /**
     * Testet das Erstellen von Events mit ungültiger AccountID-Liste
     */
    @Test
    void initiateEventInvalidAccountList() {
        String title = "title";
        List<Integer> accountIds = List.of(ACCOUNT_ID, ACCOUNT2_ID);
        Double amount = 15D;

        when(accountRepository.findByIdIn(accountIds)).thenReturn(List.of(ACCOUNT));
        assertThrows(ValidationException.class, () -> transactionService.initiateEvent(title, accountIds, amount, ADMIN));
    }

    /**
     * Testet das Erstellen von Events mit ungültigem Lehrer
     */
    @Test
    void initiateEventInvalidTeacher() {
        String title = "title";
        List<Integer> accountIds = List.of(ACCOUNT_ID, ACCOUNT2_ID);
        Double amount = 15D;

        when(accountRepository.findByIdIn(accountIds)).thenReturn(List.of(ACCOUNT, ACCOUNT2));
        assertThrows(ValidationException.class, () -> transactionService.initiateEvent(title, accountIds, amount, TEACHER));
    }

    /**
     * Testet das Erstellen von Events mit gültigen Eingaben
     */
    @Test
    void initiateEventValidAccountListValidInitiator() {
        String title = "title";
        List<Integer> accountIds = List.of(ACCOUNT_ID, ACCOUNT2_ID);
        Double amount = 15D;
        User initiator = ADMIN;

        List<Transaction> expectedTransactions = List.of(
                Transaction.builder().title(title).amount(amount).initiator(initiator).account(ACCOUNT).build(),
                Transaction.builder().title(title).amount(amount).initiator(initiator).account(ACCOUNT2).build()
        );

        when(accountRepository.findByIdIn(accountIds)).thenReturn(List.of(ACCOUNT, ACCOUNT2));
        when(transactionRepository.saveAllAndFlush(expectedTransactions)).thenReturn(expectedTransactions);
        assertThat(transactionService.initiateEvent(title, accountIds, amount, initiator)).isEqualTo(expectedTransactions);
    }
}