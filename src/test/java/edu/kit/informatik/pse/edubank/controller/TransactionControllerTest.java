package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.response.TransactionResponseDTO;
import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.Transaction;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.service.AccountService;
import edu.kit.informatik.pse.edubank.service.TransactionService;
import org.apache.tomcat.jni.Time;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von TransactionController
 *
 * @see TransactionController
 */
@ExtendWith(MockitoExtension.class)
class TransactionControllerTest {

    private static final Integer TRANSACTION_ID = 1;
    private static final Integer TRANSACTION2_ID = 2;
    private static final Integer TRANSACTION3_ID = 3;

    private static final Integer ACCOUNT_ID = 1;



    private static final User TEACHER = User.builder().id(1).role(User.UserRole.ROLE_TEACHER).build();
    private static final User STUDENT = User.builder().id(2).role(User.UserRole.ROLE_STUDENT).build();
    private static final AuthenticatedUser AUTH_USER = new AuthenticatedUser(TEACHER);

    private static final Subject SUBJECT = Subject.builder()
            .id(10)
            .build();

    private static final Account ACCOUNT = Account.builder()
            .id(ACCOUNT_ID)
            .subject(SUBJECT)
            .student(STUDENT)
            .build();

    private static final Transaction TRANSACTION = Transaction.builder()
            .id(TRANSACTION_ID)
            .account(ACCOUNT)
            .title("transaction")
            .amount(10D)
            .initiator(TEACHER)
            .createdAt(new Date())
            .build();

    private static final Transaction TRANSACTION2 = Transaction.builder()
            .id(TRANSACTION2_ID)
            .account(ACCOUNT)
            .title("transaction")
            .amount(10D)
            .initiator(TEACHER)
            .createdAt(new Date())
            .build();

    private static final Transaction TRANSACTION3 = Transaction.builder()
            .id(TRANSACTION3_ID)
            .account(ACCOUNT)
            .title("transaction")
            .amount(10D)
            .initiator(TEACHER)
            .createdAt(new Date())
            .build();

    private static final List<Transaction> TRANSACTION_LIST = List.of(
            TRANSACTION,
            TRANSACTION2,
            TRANSACTION3
    );

    private static final List<TransactionResponseDTO> TRANSACTION_LIST_DTOS = List.of(
            new TransactionResponseDTO(TRANSACTION),
            new TransactionResponseDTO(TRANSACTION2),
            new TransactionResponseDTO(TRANSACTION3)
    );

    @Mock
    private TransactionService transactionService;
    @Mock
    private AccountService accountService;

    @InjectMocks
    private TransactionController transactionController;

    /**
     * Testet das Filtern von Transaktionen mit ungültiger ID
     */
    @Test
    void findByNonexistentId() {
        when(transactionService.findById(TRANSACTION_ID)).thenReturn(null);
        assertThat(transactionController.findById(TRANSACTION_ID, AUTH_USER).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    /**
     * Testet das Filtern von Transaktionen ohne Autorisierung
     */
    @Test
    void findByIdUnauthorized() {
        when(transactionService.findById(TRANSACTION_ID)).thenReturn(TRANSACTION);
        when(accountService.hasPermission(AUTH_USER.getUser(), ACCOUNT)).thenReturn(false);
        assertThat(transactionController.findById(TRANSACTION_ID, AUTH_USER).getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Testet das Filtern von Transaktionen nach ID
     */
    @Test
    void findById() {
        when(transactionService.findById(TRANSACTION_ID)).thenReturn(TRANSACTION);
        when(accountService.hasPermission(AUTH_USER.getUser(), ACCOUNT)).thenReturn(true);
        assertThat(transactionController.findById(TRANSACTION_ID, AUTH_USER).getBody()).isEqualTo(new TransactionResponseDTO(TRANSACTION));
    }

    /**
     * Testet das Filtern von Transaktionen nach ungültiger AccountID
     */
    @Test
    void findByNonexistentAccountId() {
        when(accountService.findById(ACCOUNT_ID)).thenReturn(null);
        assertThat(transactionController.findByAccountId(ACCOUNT_ID, AUTH_USER).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    /**
     * Testet das Filtern von Transaktionen nach AccountID ohne Autorisierung
     */
    @Test
    void findByAccountIdUnauthorized() {
        when(accountService.findById(ACCOUNT_ID)).thenReturn(ACCOUNT);
        when(accountService.hasPermission(AUTH_USER.getUser(), ACCOUNT)).thenReturn(false);
        assertThat(transactionController.findByAccountId(ACCOUNT_ID, AUTH_USER).getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Testet das Filtern von Transaktionen nach AccountID
     */
    @Test
    void findByAccountId() {
        when(accountService.findById(ACCOUNT_ID)).thenReturn(ACCOUNT);
        when(accountService.hasPermission(AUTH_USER.getUser(), ACCOUNT)).thenReturn(true);
        when(transactionService.findByAccountId(ACCOUNT_ID)).thenReturn(TRANSACTION_LIST);
        assertThat(transactionController.findByAccountId(ACCOUNT_ID, AUTH_USER).getBody()).isEqualTo(TRANSACTION_LIST_DTOS);

    }
}