package edu.kit.informatik.pse.edubank.service.impl;

import edu.kit.informatik.pse.edubank.controller.AccountController;
import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von AccountServiceImpl
 *
 * @see AccountServiceImpl
 */
@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    private static final Integer STUDENT_ID = 9;
    private static final Integer STUDENT2_ID = 10;
    private static final Integer TEACHER_ID = 11;
    private static final Integer ADMIN_ID = 12;

    private static final Integer SUBJECT_ID = 20;

    private static final User STUDENT = User.builder()
            .id(STUDENT_ID)
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User STUDENT2 = User.builder()
            .id(STUDENT2_ID)
            .firstName("Firstname2")
            .lastName("lastname2")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User STUDENT3 = User.builder()
            .firstName("Firstname3")
            .lastName("lastname3")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User TEACHER = User.builder()
            .id(TEACHER_ID)
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_TEACHER)
            .build();

    private static final User ADMIN = User.builder()
            .id(ADMIN_ID)
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_ADMIN)
            .build();

    private static final Subject SUBJECT = Subject.builder()
            .id(SUBJECT_ID)
            .teacher(TEACHER)
            .build();

    private static final List<Account> ACCOUNT_LIST = List.of(
            Account.builder().id(10).student(STUDENT).subject(SUBJECT).build(),
            Account.builder().id(11).student(STUDENT2).subject(SUBJECT).build(),
            Account.builder().id(12).student(STUDENT3).subject(SUBJECT).build()
    );

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private AccountServiceImpl accountService;

    /**
     * Testet das Aufrufen von allen Konten im System
     */
    @Test
    void findAll() {
        when(accountRepository.findAll()).thenReturn(ACCOUNT_LIST);
        assertThat(accountService.findAll()).isEqualTo(ACCOUNT_LIST);
    }

    /**
     * Testet das Filtern von Konten nach ID
     */
    @Test
    void findById() {
        Integer accId = 10;
        Account account = Account.builder()
                .id(accId)
                .build();

        when(accountRepository.findById(accId)).thenReturn(Optional.of(account));
        assertThat(accountService.findById(accId)).isEqualTo(account);
    }

    /**
     * Testet das Filtern von Konten nach StudentID
     */
    @Test
    void findByStudentId() {
        Account account = Account.builder()
                .student(STUDENT)
                .build();

        when(accountRepository.findByStudentId(STUDENT_ID)).thenReturn(List.of(account));
        assertThat(accountService.findByStudentId(STUDENT_ID)).isEqualTo(List.of(account));
    }

    /**
     * Testet das Filtern von Konten nach SubjectID
     */
    @Test
    void findBySubjectId() {
        Account account = Account.builder()
                .subject(SUBJECT)
                .build();

        when(accountRepository.findBySubjectId(SUBJECT_ID)).thenReturn(List.of(account));
        assertThat(accountService.findBySubjectId(SUBJECT_ID)).isEqualTo(List.of(account));
    }

    /**
     * Testet das Überprüfung, ob ein Admin Zugriff auf das Objekt hat
     */
    @Test
    void hasPermissionAdmin() {
        assertThat(accountService.hasPermission(ADMIN, Account.builder().build())).isTrue();
    }

    /**
     *  Testet das Überprüfung, ob ein Student Zugriff auf das Objekt hat
     */
    @Test
    void hasPermissionStudentOfAccount() {
        Account account = Account.builder()
                .student(STUDENT)
                .build();

        assertThat(accountService.hasPermission(STUDENT, account)).isTrue();
    }

    /**
     *  Testet das Überprüfung, ob ein Student ohne Autorisierung Zugriff auf das Objekt hat
     */
    @Test
    void hasNoPermissionStudentNotOfAccount() {
        Account account = Account.builder()
                .student(STUDENT)
                .build();

        assertThat(accountService.hasPermission(STUDENT2, account)).isFalse();
    }

    /**
     *  Testet das Überprüfung, ob ein Lehrer Zugriff auf das Objekt hat
     */
    @Test
    void hasPermissionTeacherOfSubjectOfAccount() {
        Account account = Account.builder()
                .subject(SUBJECT)
                .build();

        assertThat(accountService.hasPermission(TEACHER, account)).isTrue();
    }
}