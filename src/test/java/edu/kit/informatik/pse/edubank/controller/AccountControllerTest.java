package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.request.TransactionRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.AccountResponseDTO;
import edu.kit.informatik.pse.edubank.dto.response.AccountTransactionsResponseDTO;
import edu.kit.informatik.pse.edubank.dto.response.TransactionResponseDTO;
import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.Transaction;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.service.AccountService;
import edu.kit.informatik.pse.edubank.service.SubjectService;
import edu.kit.informatik.pse.edubank.service.TransactionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von AccountController
 *
 * @see AccountController
 */
@ExtendWith(MockitoExtension.class)
class AccountControllerTest {

    private static final Integer STUDENT_ID = 9;
    private static final Integer STUDENT2_ID = 10;
    private static final Integer TEACHER_ID = 11;
    private static final Integer ADMIN_ID = 12;
    private static final Integer ACCOUNT_ID = 15;
    private static final Integer ACCOUNT2_ID = 16;
    private static final Integer ACCOUNT3_ID = 17;

    private static final Integer SUBJECT_ID = 20;

    private static final User STUDENT = User.builder()
            .id(STUDENT_ID)
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User STUDENT2 = User.builder()
            .id(STUDENT2_ID)
            .firstName("Firstname2")
            .lastName("lastname2")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User STUDENT3 = User.builder()
            .firstName("Firstname3")
            .lastName("lastname3")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User TEACHER = User.builder()
            .id(TEACHER_ID)
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_TEACHER)
            .build();

    private static final User ADMIN = User.builder()
            .id(ADMIN_ID)
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_ADMIN)
            .build();

    private static final Subject SUBJECT = Subject.builder()
            .id(SUBJECT_ID)
            .teacher(TEACHER)
            .build();

    private static final List<Account> ACCOUNT_LIST = List.of(
            Account.builder().id(ACCOUNT_ID).student(STUDENT).subject(SUBJECT)
                    .transactions(List.of(
                            Transaction.builder()
                                    .id(1)
                                    .title("transaction")
                                    .amount(10D)
                                    .initiator(TEACHER)
                                    .createdAt(new Date())
                                    .build(),
                            Transaction.builder()
                                    .id(1)
                                    .title("transaction")
                                    .amount(10D)
                                    .initiator(TEACHER)
                                    .createdAt(new Date())
                                    .build(),
                            Transaction.builder()
                                    .id(1)
                                    .title("transaction")
                                    .amount(10D)
                                    .initiator(TEACHER)
                                    .createdAt(new Date())
                                    .build()
                    ))
                    .build(),
            Account.builder().id(ACCOUNT2_ID).student(STUDENT2).subject(SUBJECT).build(),
            Account.builder().id(ACCOUNT3_ID).student(STUDENT3).subject(SUBJECT).build()
    );

    private static final List<Transaction> TRANSACTION_LIST = List.of(
            Transaction.builder().account(ACCOUNT_LIST.get(0)).amount(10D).build(),
            Transaction.builder().account(ACCOUNT_LIST.get(0)).amount(11D).build(),
            Transaction.builder().account(ACCOUNT_LIST.get(0)).amount(12D).build()
    );

    private static final List<TransactionResponseDTO> TRANSACTION_DTO_LIST = List.of(
            new TransactionResponseDTO(Transaction.builder().account(ACCOUNT_LIST.get(0)).amount(10D).build()),
            new TransactionResponseDTO(Transaction.builder().account(ACCOUNT_LIST.get(0)).amount(11D).build()),
            new TransactionResponseDTO(Transaction.builder().account(ACCOUNT_LIST.get(0)).amount(12D).build())
    );

    private static final List<Account> ACCOUNT_STUDENT_LIST = List.of(
            Account.builder().id(ACCOUNT_ID).student(STUDENT).subject(SUBJECT).build()
    );

    private static final List<AccountResponseDTO> ACCOUNT_RESPONSE_DTOS_LIST = List.of(
            new AccountResponseDTO(Account.builder().id(ACCOUNT_ID).student(STUDENT).subject(SUBJECT).build()),
            new AccountResponseDTO(Account.builder().id(ACCOUNT2_ID).student(STUDENT2).subject(SUBJECT).build()),
            new AccountResponseDTO(Account.builder().id(ACCOUNT3_ID).student(STUDENT3).subject(SUBJECT).build())
    );

    private static final List<AccountResponseDTO> ACCOUNT_STUDENT_RESPONSE_DTOS_LIST = List.of(
            new AccountResponseDTO(Account.builder().id(ACCOUNT_ID).student(STUDENT).subject(SUBJECT).build())
    );

    private static final AuthenticatedUser AUTH_TEACHER_USER = new AuthenticatedUser(TEACHER);
    private static final AuthenticatedUser AUTH_STUDENT_USER = new AuthenticatedUser(STUDENT);
    private static final AuthenticatedUser AUTH_ADMIN_USER = new AuthenticatedUser(ADMIN);

    @Mock
    private AccountService accountService;
    @Mock
    private SubjectService subjectService;
    @Mock
    private TransactionService transactionService;
    @Mock
    private TransactionController transactionController;

    @InjectMocks
    private AccountController accountController;

    /**
     * Testet, ob alle Accounts richtig zurückgegeben werden
     */
    @Test
    void findAll() {
        when(accountService.findAll()).thenReturn(ACCOUNT_LIST);
        assertThat(accountController.findAll().getBody()).isEqualTo(ACCOUNT_RESPONSE_DTOS_LIST);
    }

    /**
     * Testet, ob ein Fehler auftritt, wenn unatorisierter Benutzer Accounts nach SubjectId sucht
     */
    @Test
    void findBySubjectIdFromUnauthorizedTeacher() {
        when(subjectService.findById(SUBJECT_ID)).thenReturn(SUBJECT);
        when(subjectService.hasPermission(AUTH_TEACHER_USER.getUser(), SUBJECT)).thenReturn(false);
        assertThat(accountController.findBySubjectId(SUBJECT_ID, AUTH_TEACHER_USER).getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Testet, ob Accounts nach SubjectId richtig filtriert werden
     */
    @Test
    void findBySubjectId() {
        when(accountService.findBySubjectId(SUBJECT_ID)).thenReturn(ACCOUNT_LIST);
        assertThat(accountController.findBySubjectId(SUBJECT_ID, AUTH_ADMIN_USER).getBody()).isEqualTo(ACCOUNT_RESPONSE_DTOS_LIST);
    }

    /**
     * Testet, ob ein Fehler auftritt, wenn unautorisierter Benutzer nach Accounts sucht
     */
    @Test
    void findByStudentIdUnauthorized() {
        assertThat(accountController.findByStudentId(STUDENT2_ID, AUTH_STUDENT_USER).getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Testet, ob das Filtern von Accounts nach StudentID richtig funktioniert
     */
    @Test
    void findByStudentId() {
        when(accountService.findByStudentId(STUDENT_ID)).thenReturn(ACCOUNT_STUDENT_LIST);
        assertThat(accountController.findByStudentId(STUDENT_ID, AUTH_ADMIN_USER).getBody()).isEqualTo(ACCOUNT_STUDENT_RESPONSE_DTOS_LIST);
    }

    /**
     * Testet, ob ein Fehler auftritt, wenn man nach Account mit falscher ID sucht
     */
    @Test
    void findByIdNotFound() {
        when(accountService.findById(ACCOUNT_LIST.get(0).getId())).thenReturn(null);
        assertThat(accountController.findById(ACCOUNT_LIST.get(0).getId(), AUTH_ADMIN_USER).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    /**
     * Testet, ob ein Fehler auftritt, wenn man unautorisiert nach Account mit ID sucht
     */
    @Test
    void findByIdUnauthorized() {
        when(accountService.findById(ACCOUNT_LIST.get(0).getId())).thenReturn(ACCOUNT_LIST.get(0));
        when(accountService.hasPermission(any(), any())).thenReturn(false);
        assertThat(accountController.findById(ACCOUNT_LIST.get(0).getId(), AUTH_ADMIN_USER).getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Testet das Filtern von Accounts nach ID
     */
    @Test
    void findById() {
        when(accountService.findById(ACCOUNT_LIST.get(0).getId())).thenReturn(ACCOUNT_LIST.get(0));
        when(accountService.hasPermission(AUTH_ADMIN_USER.getUser(), ACCOUNT_LIST.get(0))).thenReturn(true);
        assertThat(accountController.findById(ACCOUNT_LIST.get(0).getId(), AUTH_ADMIN_USER).getBody()).isEqualTo(new AccountTransactionsResponseDTO(ACCOUNT_LIST.get(0)));
    }

    /**
     * Testet das Filtern von Transaktionen nach AccountID
     */
    @Test
    void getTransactionsById() {
        when(accountService.findById(ACCOUNT_LIST.get(0).getId())).thenReturn(ACCOUNT_LIST.get(0));
        when(transactionController.findByAccountId(ACCOUNT_LIST.get(0).getId(), AUTH_ADMIN_USER)).thenReturn(ResponseEntity.ok(TRANSACTION_DTO_LIST));
        when(accountService.hasPermission(AUTH_ADMIN_USER.getUser(), ACCOUNT_LIST.get(0))).thenReturn(true);
        assertThat(accountController.getTransactionsById(ACCOUNT_LIST.get(0).getId(), AUTH_ADMIN_USER).getBody()).isEqualTo(TRANSACTION_DTO_LIST);
    }

    /**
     * Testet das Ausführen von Transaktionen
     */
    @Test
    void executeTransaction() {
        String title = "title";
        Double amount = 10D;
        Transaction transaction = Transaction.builder().amount(amount).account(ACCOUNT_LIST.get(0)).initiator(ADMIN).title(title).build();
        TransactionRequestDTO requestDTO = TransactionRequestDTO.builder().title(title).amount(amount).build();
        TransactionResponseDTO responseDTO = new TransactionResponseDTO(transaction);
        when(accountService.findById(ACCOUNT_LIST.get(0).getId())).thenReturn(ACCOUNT_LIST.get(0));
        when(transactionService.createTransaction(transaction)).thenReturn(transaction);
        when(accountService.hasPermission(AUTH_ADMIN_USER.getUser(), ACCOUNT_LIST.get(0))).thenReturn(true);
        assertThat(accountController.executeTransaction(ACCOUNT_ID, requestDTO, AUTH_ADMIN_USER).getBody()).isEqualTo(responseDTO);

    }
}