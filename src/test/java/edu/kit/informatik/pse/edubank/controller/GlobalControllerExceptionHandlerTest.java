package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.service.impl.ValidationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Klasse für Unit-Testing von GlobalControllerExceptionHandler
 *
 * @see GlobalControllerExceptionHandler
 */
@ExtendWith(MockitoExtension.class)
class GlobalControllerExceptionHandlerTest {

    @InjectMocks
    private GlobalControllerExceptionHandler globalControllerExceptionHandler;

    /**
     * Testet das Handling von Exceptions in REST-Anfragen
     */
    @Test
    void handleResourceValidationExceptions() {
        ValidationException validationException = new ValidationException("field", "message");
        Map<String, String> expectedMap = Map.of("field", "message");
        assertThat(globalControllerExceptionHandler.handleResourceValidationExceptions(validationException)).isEqualTo(expectedMap);
    }
}