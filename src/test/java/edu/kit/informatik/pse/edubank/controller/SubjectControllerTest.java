package edu.kit.informatik.pse.edubank.controller;

import edu.kit.informatik.pse.edubank.dto.request.SubjectRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.AccountResponseView;
import edu.kit.informatik.pse.edubank.dto.response.SubjectResponseDTO;
import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.SchoolClass;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.service.SubjectService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.CLASS;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von SubjectController
 *
 * @see SubjectController
 */
@ExtendWith(MockitoExtension.class)
class SubjectControllerTest {

    private static final Integer STUDENT_ID = 8;
    private static final Integer STUDENT2_ID = 9;
    private static final Integer STUDENT3_ID = 10;
    private static final Integer TEACHER_ID = 11;
    private static final Integer ADMIN_ID = 12;

    private static final Integer SUBJECT_ID = 30;
    private static final Integer SUBJECT2_ID = 40;
    private static final Integer SUBJECT3_ID = 50;

    private static final User STUDENT = User.builder()
            .id(STUDENT_ID)
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User STUDENT2 = User.builder()
            .id(STUDENT2_ID)
            .firstName("Firstname2")
            .lastName("lastname2")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User STUDENT3 = User.builder()
            .id(STUDENT3_ID)
            .firstName("Firstname3")
            .lastName("lastname3")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User TEACHER = User.builder()
            .id(TEACHER_ID)
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_TEACHER)
            .build();

    private static final User ADMIN = User.builder()
            .id(ADMIN_ID)
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_ADMIN)
            .build();

    private static final AuthenticatedUser AUTH_USER_STUDENT = new AuthenticatedUser(STUDENT);
    private static final AuthenticatedUser AUTH_USER_TEACHER = new AuthenticatedUser(TEACHER);
    private static final AuthenticatedUser AUTH_USER_ADMIN = new AuthenticatedUser(ADMIN);

    private static final Integer CLASS_ID = 1;
    private static final SchoolClass SCHOOL_CLASS = SchoolClass.builder().id(CLASS_ID).name("class").build();

    private static final Subject SUBJECT = Subject.builder()
            .id(SUBJECT_ID)
            .teacher(TEACHER)
            .schoolClass(SCHOOL_CLASS)
            .build();

    private static final Subject SUBJECT2 = Subject.builder()
            .id(SUBJECT2_ID)
            .schoolClass(SCHOOL_CLASS)
            .build();

    private static final Subject SUBJECT3 = Subject.builder()
            .id(SUBJECT3_ID)
            .teacher(TEACHER)
            .build();

    private static final List<Subject> SUBJECT_LIST = List.of(SUBJECT, SUBJECT2, SUBJECT3);
    private static final List<Subject> SUBJECT_LIST_CLASS = List.of(SUBJECT, SUBJECT2);
    private static final List<Subject> SUBJECT_LIST_TEACHER = List.of(SUBJECT, SUBJECT3);

    private static final List<SubjectResponseDTO> SUBJECT_RESPONSE_DTO_LIST = List.of(
            new SubjectResponseDTO(SUBJECT),
            new SubjectResponseDTO(SUBJECT2),
            new SubjectResponseDTO(SUBJECT3)
    );

    private static final List<SubjectResponseDTO> SUBJECT_RESPONSE_DTO_LIST_CLASS = List.of(
            new SubjectResponseDTO(SUBJECT),
            new SubjectResponseDTO(SUBJECT2)
    );

    private static final List<SubjectResponseDTO> SUBJECT_RESPONSE_DTO_LIST_TEACHER = List.of(
            new SubjectResponseDTO(SUBJECT),
            new SubjectResponseDTO(SUBJECT3)
    );

    @Mock
    private SubjectService subjectService;

    @InjectMocks
    private SubjectController subjectController;

    /**
     * Testet das Aufrufen von allen Fächern im System
     */
    @Test
    void findAll() {
        when(subjectService.findAll()).thenReturn(SUBJECT_LIST);
        assertThat(subjectController.findAll().getBody()).isEqualTo(SUBJECT_RESPONSE_DTO_LIST);
    }

    /**
     * Testet das Filtern von Fächern nach ID mit ungültiger ID
     */
    @Test
    void findByUnexistingId() {
        when(subjectService.findById(any())).thenReturn(null);
        assertThat(subjectController.findById(0, AUTH_USER_ADMIN).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    /**
     * Testet das Filtern von Fächern nach ID mit ungültiger Autorisierung
     */
    @Test
    void findByIdUnauthorized() {
        when(subjectService.findById(any())).thenReturn(SUBJECT);
        when(subjectService.hasPermission(TEACHER, SUBJECT)).thenReturn(false);
        assertThat(subjectController.findById(0, AUTH_USER_TEACHER).getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Testet das Filtern von Fächern nach ID
     */
    @Test
    void findById() {
        when(subjectService.findById(SUBJECT_ID)).thenReturn(SUBJECT);
        when(subjectService.hasPermission(TEACHER, SUBJECT)).thenReturn(true);
        SubjectResponseDTO response = new SubjectResponseDTO(SUBJECT);
        assertThat(subjectController.findById(SUBJECT_ID, AUTH_USER_TEACHER).getBody()).isEqualTo(response);
    }

    /**
     * Testet das Filtern von Fächern nach ClassID
     */
    @Test
    void findByClassId() {
        when(subjectService.findByClassId(CLASS_ID)).thenReturn(SUBJECT_LIST_CLASS);
        assertThat(subjectController.findByClassId(CLASS_ID).getBody()).isEqualTo(SUBJECT_RESPONSE_DTO_LIST_CLASS);
    }

    /**
     * Testet das Filtern von Fächern nach TeacherID
     */
    @Test
    void findByTeacherId() {
        when(subjectService.findByTeacherId(TEACHER_ID)).thenReturn(SUBJECT_LIST_TEACHER);
        assertThat(subjectController.findByTeacherId(TEACHER_ID).getBody()).isEqualTo(SUBJECT_RESPONSE_DTO_LIST_TEACHER);
    }

    /**
     * Testet das Erstellen von Klassen mit ungültiger Autorisierung
     */
    @Test
    void createSubjectUnauthorized() {
        SubjectRequestDTO request = SubjectRequestDTO.builder().teacherId(0).build();
        assertThat(subjectController.createSubject(request, AUTH_USER_TEACHER).getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Testet das Erstellen von Klassen
     */
    @Test
    void createSubject() {
        SubjectRequestDTO request = SubjectRequestDTO.builder().teacherId(TEACHER_ID).classId(CLASS_ID).name("Subject").build();
        Subject result = Subject.builder()
                .teacher(User.builder().id(TEACHER_ID).build())
                .schoolClass(SchoolClass.builder().id(CLASS_ID).build())
                .name("Subject")
                .build();

        SubjectResponseDTO responseDTO = new SubjectResponseDTO(result);
        when(subjectService.createSubject(result)).thenReturn(result);
        assertThat(subjectController.createSubject(request, AUTH_USER_TEACHER).getBody()).isEqualTo(responseDTO);
    }

    /**
     * Testet das Aktualisierung von Klassen
     */
    @Test
    void updateSubject() {
        SubjectRequestDTO request = SubjectRequestDTO.builder().teacherId(TEACHER_ID).classId(CLASS_ID).name("Subject").build();
        Subject result = Subject.builder()
                .teacher(User.builder().id(TEACHER_ID).build())
                .schoolClass(SchoolClass.builder().id(CLASS_ID).build())
                .name("Subject")
                .build();

        SubjectResponseDTO responseDTO = new SubjectResponseDTO(result);
        when(subjectService.findById(SUBJECT_ID)).thenReturn(result);
        when(subjectService.updateSubject(SUBJECT_ID, result)).thenReturn(result);
        when(subjectService.hasPermission(TEACHER, result)).thenReturn(true);
        assertThat(subjectController.updateSubject(SUBJECT_ID, request, AUTH_USER_TEACHER).getBody()).isEqualTo(responseDTO);
    }

    /**
     * Testet das Hinzufügen von Schülern in ein Fach
     */
    @Test
    void addStudentsToSubject() {
        Subject result = Subject.builder()
                .id(SUBJECT_ID)
                .teacher(TEACHER)
                .schoolClass(SCHOOL_CLASS)
                .name("Subject")
                .build();

        SubjectResponseDTO responseDTO = new SubjectResponseDTO(result);
        when(subjectService.findById(SUBJECT_ID)).thenReturn(result);

        List<Integer> studentIds = List.of(STUDENT_ID, STUDENT2_ID);
        List<Account> accounts = List.of(
                Account.builder().subject(SUBJECT).student(STUDENT).build(),
                Account.builder().subject(SUBJECT).student(STUDENT2).build(),
                Account.builder().subject(SUBJECT).student(STUDENT3).build()
        );

        result.setAccounts(accounts);

        List<AccountResponseView> responses = List.of(
                new AccountResponseView(Account.builder().subject(SUBJECT).student(STUDENT).build()),
                new AccountResponseView(Account.builder().subject(SUBJECT).student(STUDENT2).build()),
                new AccountResponseView(Account.builder().subject(SUBJECT).student(STUDENT3).build())
        );

        when(subjectService.addStudentsToSubject(SUBJECT_ID, studentIds)).thenReturn(accounts);
        when(subjectService.hasPermission(ADMIN, result)).thenReturn(true);
        assertThat(subjectController.addStudentsToSubject(SUBJECT_ID, studentIds, AUTH_USER_ADMIN).getBody()).isEqualTo(responses);
    }

    /**
     * Testet das Löschen von Schülern aus einem Fach
     */
    @Test
    void removeStudentsFromSubject() {
        Subject result = Subject.builder()
                .id(SUBJECT_ID)
                .teacher(TEACHER)
                .schoolClass(SCHOOL_CLASS)
                .name("Subject")
                .build();

        List<Account> accounts = List.of(
                Account.builder().subject(SUBJECT).student(STUDENT).build(),
                Account.builder().subject(SUBJECT).student(STUDENT2).build(),
                Account.builder().subject(SUBJECT).student(STUDENT3).build()
        );

        result.setAccounts(accounts);

        SubjectResponseDTO responseDTO = new SubjectResponseDTO(result);
        when(subjectService.findById(SUBJECT_ID)).thenReturn(result);

        List<Integer> studentIds = List.of(STUDENT_ID, STUDENT2_ID);
        List<Account> newAccounts = List.of();

        List<AccountResponseView> responses = List.of();

        when(subjectService.removeStudentsFromSubject(SUBJECT_ID, studentIds)).thenReturn(newAccounts);
        when(subjectService.hasPermission(ADMIN, result)).thenReturn(true);
        assertThat(subjectController.removeStudentsFromSubject(SUBJECT_ID, studentIds, AUTH_USER_ADMIN).getBody()).isEqualTo(responses);
    }

    /**
     * Testet das Hinzufügen von Schülern in ein Fach ohne Autorisierung
     */
    @Test
    void removeStudentsFromSubjectUnauthorized() {
        Subject result = Subject.builder()
                .id(SUBJECT_ID)
                .teacher(TEACHER)
                .schoolClass(SCHOOL_CLASS)
                .name("Subject")
                .build();

        when(subjectService.findById(SUBJECT_ID)).thenReturn(result);

        List<Integer> studentIds = List.of(STUDENT_ID, STUDENT2_ID);
        List<Account> newAccounts = List.of();
        List<AccountResponseView> responses = List.of();

        when(subjectService.hasPermission(ADMIN, result)).thenReturn(false);
        assertThat(subjectController.removeStudentsFromSubject(SUBJECT_ID, studentIds, AUTH_USER_ADMIN).getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Testet das Löschen von Fächern
     */
    @Test
    void deleteSubject() {
        when(subjectService.findById(SUBJECT_ID)).thenReturn(SUBJECT);
        when(subjectService.hasPermission(ADMIN, SUBJECT)).thenReturn(true);
        when(subjectService.deleteSubjectById(SUBJECT_ID)).thenReturn(true);
        assertThat(subjectController.deleteSubject(SUBJECT_ID, AUTH_USER_ADMIN).getBody()).isTrue();
    }

    /**
     * Testet das Löschen von Fächern ohne Autorisierung
     */
    @Test
    void deleteSubjectUnauthorized() {
        when(subjectService.findById(SUBJECT_ID)).thenReturn(SUBJECT);
        when(subjectService.hasPermission(ADMIN, SUBJECT)).thenReturn(false);
        assertThat(subjectController.deleteSubject(SUBJECT_ID, AUTH_USER_ADMIN).getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    /**
     * Testet das Löschen von Fächern mit ungültiger ID
     */
    @Test
    void deleteSubjectInvalidID() {
        when(subjectService.findById(SUBJECT_ID)).thenReturn(null);
        assertThat(subjectController.deleteSubject(SUBJECT_ID, AUTH_USER_ADMIN).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }
}