package edu.kit.informatik.pse.edubank.controller;

import com.sun.jdi.request.EventRequest;
import edu.kit.informatik.pse.edubank.dto.request.EventRequestDTO;
import edu.kit.informatik.pse.edubank.dto.response.TransactionResponseDTO;
import edu.kit.informatik.pse.edubank.entity.Account;
import edu.kit.informatik.pse.edubank.entity.Subject;
import edu.kit.informatik.pse.edubank.entity.Transaction;
import edu.kit.informatik.pse.edubank.entity.User;
import edu.kit.informatik.pse.edubank.security.AuthenticatedUser;
import edu.kit.informatik.pse.edubank.service.TransactionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Klasse für Unit-Testing von EventController
 *
 * @see EventController
 */
@ExtendWith(MockitoExtension.class)
class EventControllerTest {

    private static final User ADMIN = User.builder()
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_ADMIN)
            .build();


    private static final User STUDENT = User.builder()
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User STUDENT2 = User.builder()
            .firstName("Firstname2")
            .lastName("lastname2")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User STUDENT3 = User.builder()
            .firstName("Firstname3")
            .lastName("lastname3")
            .role(User.UserRole.ROLE_STUDENT)
            .build();

    private static final User TEACHER = User.builder()
            .firstName("Firstname")
            .lastName("lastname")
            .role(User.UserRole.ROLE_TEACHER)
            .build();

    private static final Subject SUBJECT = Subject.builder()
            .teacher(TEACHER)
            .build();

    private static final List<Account> ACCOUNT_LIST = List.of(
            Account.builder().id(1).student(STUDENT).subject(SUBJECT).build(),
            Account.builder().id(2).student(STUDENT2).subject(SUBJECT).build(),
            Account.builder().id(3).student(STUDENT3).subject(SUBJECT).build()
    );


    private static final AuthenticatedUser AUTH_ADMIN_USER = new AuthenticatedUser(ADMIN);

    @Mock
    private TransactionService transactionService;

    @InjectMocks
    private EventController eventController;

    /**
     * Testet das Erstellen von events
     */
    @Test
    void initiateEvent() {
        String title = "title";
        List<Integer> accountIds = List.of(1, 2, 3);
        Double amount = 10D;
        User initiator = ADMIN;

        List<Transaction> eventTransactions = List.of(
                Transaction.builder().title(title).account(ACCOUNT_LIST.get(0)).amount(amount).initiator(initiator).build(),
                Transaction.builder().title(title).account(ACCOUNT_LIST.get(1)).amount(amount).initiator(initiator).build(),
                Transaction.builder().title(title).account(ACCOUNT_LIST.get(2)).amount(amount).initiator(initiator).build()
        );

        List<TransactionResponseDTO> responseDTOs = List.of(
                new TransactionResponseDTO(eventTransactions.get(0)),
                new TransactionResponseDTO(eventTransactions.get(1)),
                new TransactionResponseDTO(eventTransactions.get(2))
        );

        EventRequestDTO eventRequestDTO = EventRequestDTO.builder().accountIds(accountIds).amount(amount).title(title).build();

        when(transactionService.initiateEvent(title, accountIds, amount, AUTH_ADMIN_USER.getUser())).thenReturn(eventTransactions);
        assertThat(eventController.initiateEvent(eventRequestDTO, AUTH_ADMIN_USER).getBody()).isEqualTo(responseDTOs);
    }
}